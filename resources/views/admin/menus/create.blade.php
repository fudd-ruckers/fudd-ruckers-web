@extends('admin.app')
@section('title') {{ $pageTitle }} @endsection
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-shopping-bag"></i> {{ $pageTitle }} - {{ $subTitle }}</h1>
        </div>
    </div>
    @include('admin.partials.flash')
    <div class="row user">
        <div class="col-md-3">
            <div class="tile p-0">
                <ul class="nav flex-column nav-tabs user-tabs">
                    <li class="nav-item"><a class="nav-link active" href="#general" data-toggle="tab">General</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-9">
            <div class="tab-content">
                <div class="tab-pane active" id="general">
                    <div class="tile">
                        <form action="{{ route('admin.menu.store') }}" method="POST" role="form">
                            @csrf
                            <h3 class="tile-title">Menu Information</h3>
                            <hr>
                            <div class="tile-body">
                                <div class="form-group">
                                    <label class="control-label" for="name">Name</label>
                                    <input
                                        class="form-control @error('name') is-invalid @enderror"
                                        type="text"
                                        placeholder="Enter menu name"
                                        id="name"
                                        name="name"
                                        value="{{ old('name') }}"
                                    />
                                    <div class="invalid-feedback active">
                                        <i class="fa fa-exclamation-circle fa-fw"></i> @error('name') <span>{{ $message }}</span> @enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="category_id">Categories</label>
                                            <select name="category_id" id="category_id" class="form-control">
                                                @foreach($categories as $category)
                                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                @endforeach
                                            </select>
											<div class="invalid-feedback active">
												<i class="fa fa-exclamation-circle fa-fw"></i> @error('category_id') <span>{{ $message }}</span> @enderror
											</div>
                                        </div>
                                    </div>
                                </div>
								<div class="form-group">
                                    <label class="control-label" for="tag">Tags</label>
                                    <textarea name="tags" id="tags" rows="8" class="form-control"></textarea>
									<div class="invalid-feedback active">
										<i class="fa fa-exclamation-circle fa-fw"></i> @error('tags') <span>{{ $message }}</span> @enderror
									</div>
                                </div>
								<div class="form-group">
                                    <label class="control-label" for="short_text">Short Text</label>
                                    <textarea name="short_text" id="short_text" rows="8" class="form-control"></textarea>
									<div class="invalid-feedback active">
										<i class="fa fa-exclamation-circle fa-fw"></i> @error('short_text') <span>{{ $message }}</span> @enderror
									</div>
                                </div>
								<div class="form-group">
                                    <label class="control-label" for="details_text">Detail Text</label>
                                    <textarea name="details_text" id="details_text" rows="8" class="form-control summernote"></textarea>
									<div class="invalid-feedback active">
										<i class="fa fa-exclamation-circle fa-fw"></i> @error('details_text') <span>{{ $message }}</span> @enderror
									</div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="price">Price</label>
                                            <input
                                                class="form-control @error('price') is-invalid @enderror"
                                                type="text"
                                                placeholder="Enter product price"
                                                id="price"
                                                name="price"
                                                value="{{ old('price') }}"
                                            />
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('price') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
								<div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="addons">Add On's</label>
                                            <select name="addons[]" id="addons" class="form-control" multiple>
                                                @foreach($menus as $menu)
                                                    <option value="{{ $menu->id }}">{{ $menu->name }}</option>
                                                @endforeach
                                            </select>
											<div class="invalid-feedback active">
												<i class="fa fa-exclamation-circle fa-fw"></i> @error('addons') <span>{{ $message }}</span> @enderror
											</div>
                                        </div>
                                    </div>
                                </div>
								<div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="drinks">Drink</label>
                                            <select name="drinks[]" id="drinks" class="form-control" multiple>
                                                @foreach($menus as $menu)
                                                    <option value="{{ $menu->id }}">{{ $menu->name }}</option>
                                                @endforeach
                                            </select>
											<div class="invalid-feedback active">
												<i class="fa fa-exclamation-circle fa-fw"></i> @error('addons') <span>{{ $message }}</span> @enderror
											</div>
                                        </div>
                                    </div>
                                </div>
								<div class="form-group">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input"
                                                   type="checkbox"
                                                   id="status"
                                                   name="status"
                                                />Status
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="tile-footer">
                                <div class="row d-print-none mt-2">
                                    <div class="col-12 text-right">
                                        <button class="btn btn-success" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save Menu</button>
                                        <a class="btn btn-danger" href="{{ route('admin.menu.index') }}"><i class="fa fa-fw fa-lg fa-arrow-left"></i>Go Back</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('backend/js/plugins/select2.min.js') }}"></script>
    <script>
        $( document ).ready(function() {
            $('#addons').select2();
			$('#drinks').select2();
			
			$('.summernote').summernote({
               height: 300,
			   popover: {
				 image: [],
				 link: [],
				 air: []
			   },
			   toolbar: [
							[ 'style', [ 'style' ] ],
							[ 'font', [ 'bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear'] ],
							[ 'fontname', [ 'fontname' ] ],
							[ 'fontsize', [ 'fontsize' ] ],
							[ 'color', [ 'color' ] ],
							[ 'para', [ 'ol', 'ul', 'paragraph', 'height' ] ],
							[ 'table', [ 'table' ] ],
							[ 'insert', [ 'link'] ],
							[ 'view', [ 'undo', 'redo', 'fullscreen', 'codeview', 'help' ] ]
						]
          });
        });
    </script>
@endpush
