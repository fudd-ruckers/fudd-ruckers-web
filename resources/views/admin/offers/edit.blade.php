@extends('admin.app')
@section('title') {{ $pageTitle }} @endsection
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/js/plugins/dropzone/dist/min/dropzone.min.css') }}"/>
@endsection

@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i> {{ $pageTitle }}</h1>
        </div>
    </div>
    @include('admin.partials.flash')
    <div class="row">
        <div class="col-md-8 mx-auto">
            <div class="tile">
                <h3 class="tile-title">{{ $subTitle }}</h3>
                <form action="{{ route('admin.offer.update') }}" method="POST" role="form" enctype="multipart/form-data">
                    @csrf
                    <div class="tile-body">
                        <div class="form-group">
                            <label class="control-label" for="title">Title <span class="m-l-5 text-danger"> *</span></label>
                            <input class="form-control @error('title') is-invalid @enderror" type="text" name="title" id="title" value="{{ old('title', $targetOffer->title) }}"/>
                            <input type="hidden" name="id" value="{{ $targetOffer->id }}">
                            @error('title') {{ $message }} @enderror
                        </div>
						<div class="form-group">
                            <label class="control-label" for="details">Details <span class="m-l-5 text-danger"> *</span></label>
							<textarea class="form-control @error('details') is-invalid @enderror" name="details" id="details">{{ $targetOffer->details }}</textarea>
                            @error('details') {{ $message }} @enderror
                        </div>
						<div class="form-group">
							<label for="document">Image <span class="m-l-5 text-danger"> *</span></label>
							<div class="needsclick dropzone" id="document-dropzone">

							</div>
							 <input type="hidden" name="image" id="offer_image" value="{{$targetOffer->image}}"/>
							 @error('image') {{ $message }} @enderror
						</div>
						<div class="form-group">
							<div class="form-check">
								<label class="form-check-label">
									<input class="form-check-input"
										   type="checkbox"
										   id="offer_status"
										   name="status"
										   {{ $targetOffer->status == 1 ? 'checked' : '' }}
										/>Status
								</label>
							</div>
                        </div>
                    </div>
                    <div class="tile-footer">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update Offer</button>
                        &nbsp;&nbsp;&nbsp;
                        <a class="btn btn-secondary" href="{{ route('admin.offer.index') }}"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('scripts')

<script type="text/javascript" src="{{ asset('backend/js/plugins/dropzone/dist/min/dropzone.min.js') }}"></script>
<script>
  var uploadedDocumentMap = {}
  Dropzone.options.documentDropzone = {
    url: '{{ route('admin.offer.storeMedia') }}',
    maxFilesize: 5, // MB
	maxFiles: 1,
    acceptedFiles: ".jpeg,.jpg,.png,.gif",
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    success: function (file, response) {
		var image =response.name+'.'+response.ext;
      $('#offer_image').val(image);
      uploadedDocumentMap[file.name] = image;
    },
    removedfile: function (file) {
      file.previewElement.remove()
      var name = ''
      if (typeof file.file_name !== 'undefined') {
        name = file.file_name
      } else {
        name = uploadedDocumentMap[file.name]
      }
      $('#offer_image').val('');
    },
	  init: function() { 
			myDropzone = this;
		  var mockFile = { name: '{{$targetOffer->image}}',size: '{{$targetOffer->size}}'};

          myDropzone.emit("addedfile", mockFile);
          myDropzone.emit("thumbnail", mockFile, "{{ asset('uploads/offer/'. $targetOffer->image) }}");
          myDropzone.emit("complete", mockFile);
	}
  }
  
</script>
@endpush
