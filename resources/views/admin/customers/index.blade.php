@extends('admin.app')
@section('title') {{ $pageTitle }} @endsection
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fal fa-user"></i> {{ $pageTitle }}</h1>
            <p>{{ $subTitle }}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th class="text-center"> Name </th>
							<th class="text-center"> Email </th>
							<th class="text-center"> Mobile Number </th>
							<th style="width:100px; min-width:100px;" class="text-center text-danger"><i class="fa fa-bolt"> </i></th>
                        </tr>
                        </thead>
                        <tbody>
						@php $i=1; @endphp
							@foreach($customers as $customer)
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$customer->first_name}}&nbsp;{{$customer->last_name}}</td>
								<td>{{$customer->email}}</td>
								<td>{{$customer->mobile_number}}</td>
								<td class="text-center">
									@if(isset($customer->deleted_at) && $customer->deleted_at!=NULL)
									<a href="#" class="btn btn-sm btn-danger" onClick="activate(@php echo $customer->id; @endphp);"><i class="fa fa-key">ACTIVATE</i></a>
									@else
									<div class="btn-group" role="group" aria-label="Second group">
                                        <a href="{{ route('admin.customers.edit', $customer->id) }}" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></a>
										<a href="{{ route('admin.customers.destroy', $customer->id) }}" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
									</div>
									@endif
                                </td>
							</tr>
							@php $i++; @endphp
							@endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('backend/js/plugins/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/plugins/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">$('#sampleTable').DataTable();</script>
	<script>
	function activate(customerid)
	{
		$.ajax({
            type: "POST",
            url: '{{route('admin.customers.activate')}}',
            data:{
					"_token": "{{ csrf_token() }}",
					"customerid": customerid
				},
            success : function(text){
                window.location.reload();
            }
        });
	}
	</script>
@endpush
