@extends('admin.app')
@section('title') {{ $pageTitle }} @endsection
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fal fa-user"></i> {{ $pageTitle }}</h1>
        </div>
    </div>
    @include('admin.partials.flash')
    <div class="row">
        <div class="col-md-8 mx-auto">
            <div class="tile">
                <h3 class="tile-title">{{ $subTitle }}</h3>
                <form action="{{ route('admin.customers.update') }}" method="POST" role="form" enctype="multipart/form-data">
                    @csrf
                    <div class="tile-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
							<input type="hidden" name="id" value="{{ $user->id }}">
								<label class="control-label" for="name">Name <span class="m-l-5 text-danger"> *</span></label>
								<input class="form-control @error('name') is-invalid @enderror" type="text" name="name" id="name" value="{{ old('name', $user->name) }}"/>
								
								@error('name') {{ $message }} @enderror
							</div>
						</div>
					</div>
						<div class="form-group">
                            <label class="control-label" for="name">Email <span class="m-l-5 text-danger"> *</span></label>
                            <input class="form-control @error('email') is-invalid @enderror" type="text" name="email" id="email" value="{{ old('email', $user->email) }}"/>
							@error('email')
								<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>
						<div class="form-group">
                            <label class="control-label" for="address">Address <span class="m-l-5 text-danger"> *</span></label>
							<textarea class="form-control @error('address') is-invalid @enderror" name="address" id="address">{{ old('address', $user->address) }}</textarea>
							<input type="hidden" id="address_id" name="address_id" value={{$user->address_id}}>
                        </div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label" for="phone">Phone <span class="m-l-5 text-danger"> *</span></label>
									<input class="form-control @error('phone') is-invalid @enderror" type="text" name="phone" id="phone" value="{{ old('phone', $user->phone) }}"/>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label" for="name">Weight <span class="m-l-5 text-danger"> *</span></label>
									<input class="form-control @error('weight') is-invalid @enderror" type="text" name="weight" id="weight" value="{{ old('weight', $user->weight) }}"/>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label" for="name">Age <span class="m-l-5 text-danger"> *</span></label>
									<input class="form-control @error('age') is-invalid @enderror" type="text" name="age" id="age" value="{{ old('age', $user->age) }}"/>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label" for="name">Sex <span class="m-l-5 text-danger"> *</span></label>
									<div>
									<input type="radio" name="sex" id="male" value="male" @if($user->sex=="male") checked @endif>&nbsp;Male
									<input type="radio" name="sex" id="female" value="female" @if($user->sex=="female") checked @endif>&nbsp;Female
									</div>
								</div>
							</div>
						</div>
                    </div>
                    <div class="tile-footer">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save Customer</button>
                        &nbsp;&nbsp;&nbsp;
                        <a class="btn btn-secondary" href="{{ route('admin.customers.index') }}"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('scripts')

@endpush
