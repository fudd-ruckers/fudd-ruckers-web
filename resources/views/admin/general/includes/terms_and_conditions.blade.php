<div class="tile">
    <form action="{{ route('admin.general.update') }}" method="POST" role="form">
        @csrf
        <h3 class="tile-title">Terms and Conditions</h3>
        <hr>
        <div class="tile-body">
            <div class="form-group">
                <label class="control-label" for="footer_copyright_text">Terms and Conditions Text</label>
                <textarea
                    class="form-control summernote"
                    rows="4"
                    placeholder="Enter terms and conditions text"
                    id="terms_and_conditions_text"
                    name="terms_and_conditions_text"
                >
					@if(isset($general))
					{{$general->terms_conditions}}
					@endif</textarea>
            </div>
        </div>
        <div class="tile-footer">
            <div class="row d-print-none mt-2">
                <div class="col-12 text-right">
                    <button class="btn btn-success" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update Terms and Conditions</button>
                </div>
            </div>
        </div>
    </form>
</div>
