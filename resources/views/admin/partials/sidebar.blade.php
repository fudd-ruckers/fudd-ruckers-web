<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
    <ul class="app-menu">
        <li>
            <a class="app-menu__item {{ Route::currentRouteName() == 'admin.dashboard' ? 'active' : '' }}" href="{{ route('admin.dashboard') }}">
                <i class="app-menu__icon fa fa-dashboard"></i>
                <span class="app-menu__label">Dashboard</span>
            </a>
        </li>
		<li>
            <a class="app-menu__item {{ Route::currentRouteName() == 'admin.customers.index' ? 'active' : '' }}" href="{{ route('admin.customers.index') }}">
                <i class="app-menu__icon fa fal fa-user"></i>
                <span class="app-menu__label">Customers</span>
            </a>
        </li>
        <li>
            <a class="app-menu__item {{ Route::currentRouteName() == 'admin.orders.index' ? 'active' : '' }}" href="{{ route('admin.orders.index') }}">
                <i class="app-menu__icon fa fa-bar-chart"></i>
                <span class="app-menu__label">Orders</span>
            </a>
        </li>
        <li>
            <a class="app-menu__item {{ Route::currentRouteName() == 'admin.menu.index' ? 'active' : '' }}" href="{{ route('admin.menu.index') }}">
                <i class="app-menu__icon fa fa-shopping-bag"></i>
                <span class="app-menu__label">Menu</span>
            </a>
        </li>
        <li>
            <a class="app-menu__item {{ Route::currentRouteName() == 'admin.categories.index' ? 'active' : '' }}" href="{{ route('admin.categories.index') }}">
                <i class="app-menu__icon fa fa-tags"></i>
                <span class="app-menu__label">Categories</span>
            </a>
        </li>
        <li>
            <a class="app-menu__item {{ Route::currentRouteName() == 'admin.attributes.index' ? 'active' : '' }}" href="{{ route('admin.attributes.index') }}">
                <i class="app-menu__icon fa fa-th"></i>
                <span class="app-menu__label">Attributes</span>
            </a>
        </li>
		<li>
            <a class="app-menu__item {{ Route::currentRouteName() == 'admin.voucher.index' ? 'active' : '' }}" href="{{ route('admin.voucher.index') }}">
                <i class="app-menu__icon fa fa-gift"></i>
                <span class="app-menu__label">Voucher</span>
            </a>
        </li>
		<li>
            <a class="app-menu__item {{ Route::currentRouteName() == 'admin.offer.index' ? 'active' : '' }}" href="{{ route('admin.offer.index') }}">
                <i class="app-menu__icon fa fa-image"></i>
                <span class="app-menu__label">Offers</span>
            </a>
        </li>
        <li>
            <a class="app-menu__item {{ Route::currentRouteName() == 'admin.settings' ? 'active' : '' }}" href="{{ route('admin.settings') }}">
                <i class="app-menu__icon fa fa-cogs"></i>
                <span class="app-menu__label">Settings</span>
            </a>
        </li>
		<!--<li>
            <a class="app-menu__item {{ Route::currentRouteName() == 'admin.general' ? 'active' : '' }}" href="{{ route('admin.general') }}">
                <i class="app-menu__icon fa fa-pencil"></i>
                <span class="app-menu__label">General Contents</span>
            </a>
        </li>-->
    </ul>
</aside>
