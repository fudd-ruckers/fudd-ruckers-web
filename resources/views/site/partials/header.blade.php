<ul class="nav nav-tabs pt-30" id="myTab" role="tablist">
	<!--<li class="nav-item">
		<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">WORLD’S GREATEST BURGER</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">APPETIZER</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">PLATTER</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">SOUP & WRAPS</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">BREAKFAST</a>
	</li>-->
	@if(isset($category))
		@foreach($category as $categories)
			<li class="nav-item">
				<a class="nav-link {{ $loop->first ? 'active' : '' }}" id="{{$categories->slug}}-tab" data-toggle="tab" href="#{{$categories->slug}}" role="tab" aria-controls="{{$categories->slug}}" aria-selected="true">{{$categories->name}}</a>
			</li>
		@endforeach
	@endif
</ul>