<aside>
	<div class="sidebar left fliph">
		<ul class="list-sidebar bg-defoult">
			<li> <a href="#" class="button-left"><span class="fa fa-fw fa-bars "></span></a> </li>
			<li> <a href="{{ route('account.profile') }}"><i class="fa fa-user" aria-hidden="true"></i> <span class="nav-label">My
						Account</span></a>
			</li>
			<li> <a href="{{url('/')}}"><i class="fa fa-home" aria-hidden="true"></i> <span class="nav-label">Home</span>
				</a></li>
			<li> <a href="{{ url('/history') }}"><i class="fa fa-history" aria-hidden="true"></i> <span class="nav-label">History</span></a>
			</li>
			<li> <a href="{{ url('/menu') }}"><i class="fa fa-cutlery" aria-hidden="true"></i> <span class="nav-label">Menu</span></a>
			</li>
			<li> <a href="{{ url('/cart') }}"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <span class="nav-label">My
						Cart</span></a> </li>
			<li> <a href="#"><i class="fa fa-percent" aria-hidden="true"></i> <span class="nav-label">Offers</span></a>
			</li>
			<li> <a href="#"><i class="fa fa-phone" aria-hidden="true"></i> <span class="nav-label">Customer
						Support</span></a> </li>
			@guest
			
			@else
			<li> <a href="{{ route('logout') }}" onclick="event.preventDefault();
											 document.getElementById('logout-form').submit();"><i class="fa fa-sign-out" aria-hidden="true"></i> <span class="nav-label">Logout</span></a>
			<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
				@csrf
			</form>
			</li>
			@endguest
		</ul>
	</div>
</aside>