@extends('site.app')
@section('title', 'Menu')
@section('content')
<div class="main-content-wrap product-listing">
	<div class="menu-wrapper pb-60">
		@include('site.partials.header')
		<div class="search-cart">
			<a href="#">
				<i class="fa fa-search" aria-hidden="true"></i>
			</a>
			<!--<a href="#" class="cart-count" data-toggle="modal" data-target="#exampleModal">-->
			<a href="#" class="cart-count" id="show_cart">
                <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                <span class="count" id="total_cart_count">{{$cartCount}}</span>
            </a>
			@include('site.pages.partials.cart')
		</div>
		<div class="tab-content pt-60" id="myTabContent">
				@if(isset($category))
					@foreach($category as $categories)
						<div class="tab-pane fade show {{ $loop->first ? 'active' : '' }}" id="{{$categories->slug}}" role="tabpanel" aria-labelledby="{{$categories->slug}}-tab">
							<h1 class="pb-20">{{$categories->name}}</h1>
							<div class="row">
								@foreach($categories->menus as $key=>$menus)
								<div class="col-md-3 mb-30">
									<div class="card product-wrap">
										@if(isset($menus->images[0]->media_file))
											<div class="image-wrap" style="background: url({{ asset('/uploads/'.$menus->images[0]->media_file)}})no-repeat;">
											<a href="#" class="view-ar">View ar</a>
											</div>
										@else
											<div class="image-wrap">
											<a href="#" class="view-ar">View ar</a>
											</div>
										@endif
										<div class="card-body">
										@if($menus->attributes->isNotEmpty())
											<h5 class="card-title" data-toggle="modal" data-target="#menuItembig_{{$menus->id}}">{{$menus->name}}</h5>
											@include('site.pages.partials.addon_popup',['id'=>$menus->id,'menu'=>$menus,'attributes'=>$attributes])
										@else
											<h5 class="card-title" data-toggle="modal" data-target="#menuItemsmall_{{$menus->id}}">{{$menus->name}}</h5>
											@include('site.pages.partials.popup',['id'=>$menus->id,'menu'=>$menus,'attributes'=>$attributes])
										@endif
										<p class="card-text">{!!$menus->short_text!!}</p>
										</div>
										<div class="card-footer">
											<span class="price">{{config('settings.currency_code')." ".$menus->price}}</span>
											<a href="javascript:void(0)" onclick="addTocart({{$menus->id}})" class="round-btn btn btn--primary">
												<i class="fa fa-plus" aria-hidden="true"></i>
											</a>
										</div>
									</div>
								</div>
								@endforeach
							</div>
						</div>
					@endforeach
				@endif
		</div>
	</div>
</div>
@endsection
@push('scripts')
<script>
	$(document).ready(function() {
		$("#show_cart").click(function() {
			$(this).addClass('showcart-trigger-clicked'); 
			var options = {
			  'backdrop': 'static'
			};
			$('#cart-popup').modal(options)
		});

		// on modal show
		$('#cart-popup').on('show.bs.modal', function() {
			var el = $(".showcart-trigger-clicked"); 
			var dv,item;
			$("#items_div").empty();
			$.ajax({
				method: "GET",
				url: "{{ url('cart-json') }}",
				success: (response) => {
					var items = response.data.cart.items; 
					var subtotal = response.data.cart.subtotal;
					var tax = response.data.cart.tax;
					var payable = response.data.cart.payable;
					$.each(items, function(index, value){
						dv = '<div class="row cart-product mb-30" id="cart_item_'+index+'"><div class="col-5 name-details"><strong>'+value.name+'</strong><p class="mb-0 pl-2">'+value.short_text+'</p><p class="mb-0 pl-2"></p></div>'
							+'<div class="col-2 pl-0 pr-0"><div class="number"><span class="minus" onclick="decrementItem('+index+')">-</span> <input type="text" value="'+value.quantity+'" id="item_quantity_'+index+'" readonly /> <span class="plus"  onclick="incremetItem('+index+')">+</span></div></div>'
							+'<div class="col-5 text-right"><div class="price">{{config("settings.currency_code")}} '+value.price+'</div><a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a><a href="#"><i class="fa fa-trash" aria-hidden="true"></i></a></div></div>';	 
						$(dv).appendTo("#items_div");	 
					});
					$("#cart_subtotal").html(subtotal); 
					$("#cart_tax").html(tax); 
					$("#cart_payable").html(payable); 
				},
				error: (response) => {
					$.notify("Something went wrong!");
				}
			})

		});

		// on modal hide
		$('#cart-popup').on('hide.bs.modal', function() {
			$('.showcart-trigger-clicked').removeClass('.showcart-trigger-clicked');
		});

	});
	function incremetItem(index){
			var input = $('#item_quantity_'+index);
			var oldValue = parseFloat(input.val());
			var newVal = oldValue + 1;
			
			$.ajax({
					method: "POST",
					url: "{{ route('increment.cart') }}",
					data: {
						"_token": "{{ csrf_token() }}",
						"cartItemIndex": index
					},
					success: (response) => {
						$('#item_quantity_'+index).val(newVal);
						$('#total_cart_count').html(response.cartCount);
						$('#cart_subtotal').html(response.subtotal);
						$('#cart_tax').html(response.tax);
						$('#cart_payable').html(response.payable);

						
					},
					error: (response) => {
						alert('Something went wrong!');
					}
				})
				
		}
		
	function decrementItem(index){
			var quantity = $('#item_quantity_'+index).val();
			if(quantity == 1)
				$('#cart_item_'+index).remove();
				
			var input = $('#item_quantity_'+index);
			var oldValue = parseFloat(input.val());	
			
			var newVal = oldValue - 1;
			
				$.ajax({
					method: "POST",
					url: "{{ route('decrement.cart') }}",
					data: {
						"_token": "{{ csrf_token() }}",
						"cartItemIndex": index
					},
					success: (response) => {
						$('#item_quantity_'+index).val(newVal);
						
						$('#total_cart_count').html(response.cartCount);
						$('#cart_subtotal').html(response.subtotal);
						$('#cart_tax').html(response.tax);
						$('#cart_payable').html(response.payable);
						quantity = $('#item_quantity_'+index).val();
						if(quantity == 0)
						$('#cart_item_'+index).remove();
					},
					error: (response) => {
						alert('Something went wrong!');
					}
				})
		}
</script>
<script>
    $(".nav li").on("click", function() {
      $(".nav li").removeClass("active");
      $(this).addClass("active");
    });
	
	function addTocart(menuId)
	{
		
		var menu_qty = $("#menu_qty_"+menuId).val();
		var special_request=$("#email_"+menuId).val();
		
		var addons=[];
		var drinks=[];
		var i=0;
		
		$('input[name=addons]:checked').map(function()
        {
            addons[i]=$(this).val();
			$(this).prop('checked', false);
			i=i+1;
        }).get();
		
		i=0;
		$('input[name=drinks]:checked').map(function()
        {
            drinks[i]=$(this).val();
			$(this).prop('checked', false);
			i=i+1;
        }).get();
		
		var attributes=[];
		$.each($("input[type=radio]:checked"), function(index, value){
			attributes.push([$(this).attr('data-id'), $(this).val()]); 
			$(this).prop('checked', false);
		});
		$("#menuItembig_"+menuId).modal("hide");
		
		$.ajax({
				method: "POST",
				url: "{{ route('add.cart') }}",
				data: {
					"_token": "{{ csrf_token() }}",
					"menuId": menuId,
					"qty":menu_qty,
					"special_request":special_request,
					"addons":addons,
					"drinks":drinks,
					"attributes":attributes
				},
				success: (response) => {
					var cartCount=response.cartCount;
					$('#total_cart_count').html(cartCount);
					//$.notify("Item added to the cart successfully");
					$.notify({
						message: 'Item added to the cart successfully',
						icon: 'flaticon-shopping-cart'
					},{
						type: 'success',
						allow_dismiss: true,
						placement: {
							from: "bottom",
							align: "right"
						},
					});
				},
				error: (response) => {
					$.notify("Something went wrong!");
				}
			})
	}
	
</script>
@endpush