<!-- Modal -->
<div class="modal fade" id="cart-popup" tabindex="-1" role="dialog" aria-labelledby="cart-popup-label" aria-hidden="true">
	<div class="modal-dialog cart-pop" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="cart-popup-label">Your Cart</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body pb-0">
				<div id="items_div">
				
				</div>
				<hr>
				<h5 class="modal-title pb-30" id="exampleModalLabel">Payment Details</h5>
				<div class="row pb-30">
					<div class="col-6">
						<button type="button" class="btn btn--secondary btn-block">REDEEM
							POINTS</button>
					</div>
					<div class="col-6">
						<button type="button" class="btn btn--secondary btn-block">VOUCHER
							CODE</button>
					</div>
				</div>
				<div class="row mb-15">
					<div class="col-6">Subtotal</div>
					<div class="col-6 text-right" id="cart_subtotal"></div>
				</div>
				<div class="row mb-15">
					<div class="col-6">Tax</div>
					<div class="col-6 text-right" id="cart_tax"></div>
				</div>
				<div class="row mb-15 total">
					<div class="col-6"><strong>Total Amount</strong></div>
					<div class="col-6 text-right"><strong id="cart_payable"></strong></div>
				</div>
			</div>
			<div class="modal-footer">
				<a href="{{ url('/cart') }}" class="btn btn--primary btn-block">CHECKOUT</a>
			</div>
		</div>
	</div>
</div>