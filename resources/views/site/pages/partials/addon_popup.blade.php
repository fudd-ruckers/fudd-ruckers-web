<!-- Modal -->
<div class="modal fade" id="menuItembig_{{$id}}" tabindex="-1" role="dialog" aria-labelledby="menuItembigTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered menu-items" role="document">
		<div class="modal-content">
			<div class="modal-header p-0">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-4">
						<div class="img-wrap">
							<a href="#" class="view-ar">View ar</a>
							@if(isset($menu->images[1]->media_file))
								<img src="{{ asset('/uploads/'.$menu->images[1]->media_file)}}" class="img-fluid mb-30" />
							@else
								<img src="" class="img-fluid mb-30" />
							@endif
						</div>
						<h3 class="pb-20">{{$menu->name}}</h3>
						<p>
						{!!$menu->details_text!!}
						</p>
					</div>
					<div class="col-md-4">
						
						@foreach($attributes as $key=>$attribute)
							@php
								if(count($menu->attributes) > 0) {
									$attributeCheck = in_array($attribute->id, $menu->attributes->pluck('attribute_id')->toArray());
									} else {
									$attributeCheck = [];
									}
							@endphp
							@if($attributeCheck)
								<hr>
								<h4>Your Choice of {{ $attribute->name }}</h4>
								<p class="mb-30">
									Choose the items from the list
								</p>
								@foreach($menu->attributes as $attributeValue)
									@if ($attributeValue->attribute_id == $attribute->id)
										@if($attributeValue->price > 0)
											<div class="custom-control custom-radio mb-30">
												<input type="radio" id="customRadio1_{{$attributeValue->value}}_{{$id}}" name="attributes_{{$key}}" value="{{ $attributeValue->id }}" class="custom-control-input" data-id="{{$attribute->id}}">
												<label class="custom-control-label" for="customRadio1_{{$attributeValue->value}}_{{$id}}">{{ ucwords($attributeValue->value) }}<span>{{config('settings.currency_code')." ".$attributeValue->price}}
														</span></label>
											</div>
										@else
											<div class="custom-control custom-radio mb-30">
												<input type="radio" id="customRadio1_{{$attributeValue->value}}_{{$id}}" name="attributes_{{$key}}" value="{{ $attributeValue->id }}" class="custom-control-input" data-id="{{$attribute->id}}">
												<label class="custom-control-label" for="customRadio1_{{$attributeValue->value}}_{{$id}}">{{ucwords($attributeValue->value)}}<span>BHD
														XXX.XX</span></label>
											</div>
										@endif	
									@endif	
								@endforeach
							@endif
						@endforeach
					</div>
					<div class="col-md-4">
						<hr>
						
						@if(!$menu->addons->isEmpty())
						<h4>Add On's1</h4>
						<p class="mb-30">
							Choose the items from the list
						</p>
						@foreach($menu->addons as $addon)
						<div class="form-check mb-30">
							<input type="checkbox" class="form-check-input" name="addons" id="addons_{{$addon->id}}" value="{{$addon->id}}" data-price="{{$addon->price}}">
							<label class="form-check-label" for="addons_{{$addon->id}}">{{$addon->name}}<span>{{config('settings.currency_code')." ".$addon->price}}</span></label>
						</div>
						@endforeach
						<!--<div class="form-check mb-30">
							<input type="checkbox" class="form-check-input" id="exampleCheck2_{{$id}}">
							<label class="form-check-label" for="exampleCheck2_{{$id}}">Add on 2<span>BHD
									XXX.XX</span></label>
						</div>-->
						<hr>
						@endif
						@if(!$menu->drinks->isEmpty())
						<h4>Your Drink</h4>
						<p class="mb-30">
							Choose the items from the list
						</p>
						@foreach($menu->drinks as $drink)
						<div class="form-check mb-30">
							<input type="checkbox" class="form-check-input" name="drinks" id="drinks_{{$drink->id}}" value="{{$drink->id}}">
							<label class="form-check-label" for="drinks_{{$drink->id}}">{{$drink->name}}<span>{{config('settings.currency_code')." ".$drink->price}}</span></label>
						</div>
						@endforeach
						<!--<div class="form-check mb-30">
							<input type="checkbox" class="form-check-input" id="exampleCheck4_{{$id}}">
							<label class="form-check-label" for="exampleCheck4_{{$id}}">Coffee<span>BHD
									XXX.XX</span></label>
						</div>-->
						@endif
						<hr>
						<h4>Special Request ? <span>(Optional)</span></h4>
						<form class="pt-20">
							<div class="form-group">
								<input type="email" class="form-control" id="email_{{$menu->id}}" aria-describedby="emailHelp" placeholder="Enter email">
							</div>
						</form>
						<div class="number">
							<span class="minus">-</span>
							<input type="text" value="1" name="qty" id="menu_qty_{{$menu->id}}"/>
							<span class="plus">+</span>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<span class="mr-auto total">{{config('settings.currency_code')." ".$menu->price}}</span>
				<button type="submit" class="btn btn--primary pl-100 pr-100" onclick="addTocart({{$menu->id}});">ADD
					TO
					CART</button>
			</div>
		</div>
	</div>
</div>