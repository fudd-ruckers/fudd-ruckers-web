<!-- Modal -->
<div class="modal fade" id="menuItemsmall_{{$id}}" tabindex="-1" role="dialog" aria-labelledby="menuItembigTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered menu-items menu-small" role="document">
		<div class="modal-content">
			<div class="modal-header p-0">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<div class="img-wrap">
							<a href="#" class="view-ar">View ar</a>
							@if(isset($menu->images[1]))
								<img src="{{ asset('/uploads/'.$menu->images[1]->media_file)}}" class="img-fluid" style="width: 100%;" />
							@else
								<img src="" class="img-fluid" style="width: 100%;" />
							@endif
						</div>

					</div>
					<div class="col-md-6">
						<h3 class="pb-20">{{$menu->name}}</h3>
						<p>
							{!!$menu->details_text!!}
						</p>
						<span class="mt-40 total">{{config('settings.currency_code')." ".$menu->price}}</span>
						<hr>
						<h4 class="pt-20 pb-20">Special Request ?
							<span>(Optional)</span>
						</h4>
						<form>
							<div class="form-group">

								<input type="email" class="form-control" id="email_{{$menu->id}}" aria-describedby="emailHelp" placeholder="Enter email">
							</div>
						</form>
						<div class="number">
							<span class="minus">-</span>
							<input type="text" value="1" name="qty" id="menu_qty_{{$menu->id}}"/>
							<span class="plus">+</span>
						</div>
						<hr>
						<button type="button" class="btn btn--primary btn-lg btn-block" onclick="addTocart({{$menu->id}});">ADD
							TO
							CART</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>