@extends('site.app')
@section('title', 'Homepage')
@section('content')
    <div class="main-content-wrap">
            <section class="slider-wrapper">
                <div class="slider singleslide">
                    <a href="#">
                        <div style="background: url({{ asset('frontend/images/banner.jpg')}})no-repeat;"></div>
                    </a>
                    <a href="#">
                        <div style="background: url({{ asset('frontend/images/banner.jpg')}})no-repeat;"></div>
                    </a>
                    <a href="#">
                        <div style="background: url({{ asset('frontend/images/banner.jpg')}})no-repeat;"></div>
                    </a>
                    <a href="#">
                        <div style="background: url({{ asset('frontend/images/banner.jpg')}})no-repeat;"></div>
                    </a>
                </div>
            </section>
            <div class="container-fluid three-ads-wrap">
                <div class="row">
                    <div class="col-md-4 box-wrap-blue" style="background:#015a9c url({{asset('frontend/images/burger.png')}})no-repeat;">
                        <div class="box-content-menu">
                            <h1 class="pb-40"><span>WORLD'S</span> <span>GREATEST</span> <span>BURGERS</span></h1>
                            <a href="#" class="primary-link">Our Menu <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 box-wrap-red" style="background:#ce1040 url({{asset('frontend/images/offer2.png')}})no-repeat;">
                        <div class="box-content-menu ml-auto">
                            <h1 class="pb-20">
                                <img src="{{asset('frontend/images/leaf.png')}}" class="img-fluid" />
                                <span>
                                    SEE OUR </span> <span>OFFERS</span>
                            </h1>
                            <a href="#" class="primary-link">Our Promos <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-2 contact-wrap">
                        <div class="contact">
                            <h1 class="pb-20">CONTACT US</h1>
                            <p class="text-center mb-0">+123 4567 8900 | +123 4567 8900</p>
                            <p class="text-center pb-20">customersuppoert@email.com</p>
                            <div class="social-media text-center">
                                <a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                @fuddruckersbh
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@stop
