@extends('site.app')
@section('title', 'Menu')
@section('content')
<div class="main-content-wrap product-listing">
	<div class="menu-wrapper payment-done pb-80 pt-80">
		<div class="row">
			<div class="col-md-8 text-center m-auto">
				<img src="{{ asset('frontend/images/tray.png')}}" class="img-fluid m-auto d-block" />
				<h1 class=" mt-30">Complete!</h1>
				<p class=" mt-30">
					Your Order is being prepared,<br> You can track your item on the main menu.
				</p>
				<div class="border-padding mt-30">
					<div class="row">
						<div class="col-md-6 text-left">
							<p>
								<strong>
									Deliver To:
								</strong>
								{{$profile->first_name." ".$profile->last_name}}<br> +973 {{$profile->mobile_number}}
								@if(isset($profile->address[0]))
									{{$profile->address[0]->flat_number}},<br> {{$profile->address[0]->road_number}},<br> {{$profile->address[0]->area}},
								@endif
							</p>
						</div>
						<div class="col-md-6 text-left">
							<p>
								<strong>
									Order Info:
								</strong>
								#{{$order->order_id}}<br>
								{{date('H:i a', strtotime($order->created_at))}}<br> {{date('F d, Y', strtotime($order->created_at))}}
							</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 mt-30">
						<p><strong>Need help?</strong> You may Contact Us<br> +123 1234 5678</p>
					</div>
					<a href="{{url('/menu')}}" class="btn btn--primary m-auto pl-60 pr-60">Done</a>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
