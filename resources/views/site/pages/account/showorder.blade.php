@extends('site.app')
@section('title', 'Order Details')
@section('content')
    <section class="top-products-area pt-100 pb-100">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="row mb-4">
                        <div class="col-6">
                            <h2 class="page-header"><i class="fa fa-globe"></i> {{ $order->order_number }}</h2>
                        </div>
                        <div class="col-6">
                            <h5 class="text-right">Date: {{ $order->created_at->toFormattedDateString() }}</h5>
                        </div>
                    </div>
					<div class="row invoice-info">
                        <div class="col-4">Placed By
                            <address><strong>{{ $order->user->name }}</strong><br>Email: {{ $order->user->email }}</address>
                        </div>
                        <div class="col-4">
                            <b>Order ID:</b> {{ $order->order_id }}<br>
                            <b>Amount:</b> {{ config('settings.currency_symbol') }}{{ round($order->value, 2) }}<br>
                            <b>Payment Method:</b> {{ $order->delivery_type }}<br>
                            <b>Payment Status:</b> {{ $order->payment_status == 1 ? 'Completed' : 'Not Completed' }}<br>
                            <b>Order Status:</b> {{ $order->status }}<br>
                        </div>
                    </div>
				</div>
                <div class="col-sm-12">
					<div class="table-responsive">
						<table class="table table-hover">
							<thead>
                                <tr>
                                    <th>#</th>
                                    <th>Product</th>
                                    <th>Qty</th>
                                    <th>Subtotal</th>
                                </tr>
                            </thead>
							<tbody>
								@php $i=1; @endphp
								@foreach($order->items as $item)
									<tr>
										<td>{{ $i }}</td>
										<td>{{ $item->product->name }}</td>
										<td>{{ $item->quantity }}</td>
										<td>{{ config('settings.currency_symbol') }}{{ round($item->value, 2) }}</td>
									</tr>
									@php $i++; @endphp
                                @endforeach
							</tbody>
						</table>
					</div>
                </div>
            </div>
		</div>
	</section>
@stop
