@extends('site.app')
@section('title', 'My Profile')
@section('content')
    <section class="register-area ptb-100">
		<div class="container">
			<div class="row">
				<div class="app-title">
				
			</div>
            </div><br>
            <div class="row">
                <main class="col-sm-12">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Order No.</th>
                                <th scope="col">Order Amount</th>
								<th scope="col">Payment Status</th>
                                <th scope="col">Status</th>
								<th style="width:100px; min-width:100px;" class="text-center text-danger"><i class='bx bxs-bolt'> </i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($orders as $order)
                                <tr>
                                    <th scope="row">{{ $order->order_id }}</th>
                                    <td>{{ config('settings.currency_symbol') }}{{ round($order->value, 2) }}</td>
									<td>@if($order->payment_status==0) <span class="badge badge-danger">Not Completed</span> @else <span class="badge badge-success">Completed</span> @endif</td>
                                    <td><span class="badge badge-success">{{ strtoupper($order->status) }}</span></td>
									<td class="text-center">
										<div class="btn-group" role="group" aria-label="Second group">
											<a href="{{ route('account.orders.view', $order->order_id) }}" class="btn btn-sm btn-primary"><i class='bx bx-show' ></i></a>
										</div>
									</td>
                                </tr>
                            @empty
                                <div class="col-sm-12">
                                    <p class="alert alert-warning">No orders to display.</p>
                                </div>
								<br>
                            @endforelse
                        </tbody>
                    </table>
                </main>
            </div>
        </div>
    </section>
	
@stop
@push('scripts')
<script>
$( document ).ready(function() {
    $('input').attr('autocomplete','off');
});
</script>
@endpush