@extends('site.app')
@section('title', 'Homepage')
@section('content')
<div class="main-content-wrap product-listing">
	<div class="menu-wrapper account pb-80 pt-60">
		<div class="row">
			<div class="col-md-12 text-right">
				<a href="{{route('account.edit')}}"><button type="button" class="btn btn--primary pl-30 pr-30">EDIT INFORMATION</button></a>
			</div>
			<div class="col-md-6 pro-details">
			@if($profile->image!=NULL)
				@php $path=$profile->image; @endphp
				<div class="pro-pic" style="background: url({{ asset('uploads/'.$path)}}) no-repeat;"></div>
			@else
				<div class="pro-pic" style="background: url({{ asset('frontend/images/pro.png')}}) no-repeat;"></div>
			@endif
				<span>
					<h3>{{$profile->first_name." ".$profile->last_name}}</h3>
					<!--<p>San Francisco, CA</p>-->
					<p class="points"><img src="{{ asset('frontend/images/coins.png')}}" /> {{$profile->no_of_stamps}} pts</p>
				</span>
			</div>
			<div class="col-md-6 contact">
				<p class="pt-10">
					<i class="fa fa-phone" aria-hidden="true"></i>+973 {{$profile->mobile_number}}
					<span>Mobile</span>
				</p>
				<p>
					<i class="fa fa-envelope" aria-hidden="true"></i>
					{{$profile->email}}
					<span>Email Address</span>
				</p>
				<a href="{{route('account.password')}}"><button type="button" class="btn btn--secondary"><i class="fa fa-lock" aria-hidden="true"></i>
					Change Password</button></a>
			</div>
		</div>
		<hr class="mt-30 mb-30">
		<div class="row account-tabs-area">
			<ul class="nav nav-tabs" id="myTab" role="tablist">
				<li class="nav-item">
					<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><i class="fa fa-home" aria-hidden="true"></i>
						Saved Address</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false"><i class="fa fa-google-wallet" aria-hidden="true"></i>
						My Wallet</a>
				</li>
				<li class="nav-item">
					<a class="nav-link border-none" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false"><i class="fa fa-money" aria-hidden="true"></i>
						Point History</a>
				</li>
			</ul>
			<div class="tab-content mt-40" id="myTabContent">
				<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
					<div class="col-md-12 list-wrap mb-40">
						<a href="{{route('account.address')}}"><button type="button" class="btn btn--primary ml-auto pl-30 pr-30">ADD ADDRESS</button></a>
					</div>
					@foreach($profile->address as $address)
					<div class="col-md-12 list-wrap mb-40">
						<i class="fa fa-map-marker" aria-hidden="true"></i>
						<div class="address">
							<h4>{{$address->address_alias}}</h4>
							<p class="mb-0">
								{{$address->flat_number.". ".$address->floor_number." ".$address->block_number." ".$address->road_number}}<br>
								{{$address->additional_information}}
							</p>
						</div>
						<div class="card-type">
							@if($address->is_default == 1) 
								Default 
							@else
								<a href="#" onclick="setDefault({{$address->id}})">Set as Default</a>
							@endif
						</div>
						<div class="edit ml-auto">
							<a href="{{route('account.address.edit',$address->id)}}">
								<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
							</a>
							@if($address->is_default == 0) 
							<a href="{{route('account.address.delete',$address->id)}}">
								<i class="fa fa-trash" aria-hidden="true"></i>
							</a>
							@endif
							
						</div>
					</div>
					@endforeach
				</div>
				<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
					<div class="col-md-12 list-wrap mb-40">
						<button type="button" class="btn btn--primary ml-auto pl-30 pr-30">ADD ADDRESS</button>
					</div>
					<div class="col-md-12 list-wrap mb-40">
						<div class="address">
							<h4>Card Name</h4>
							<p class="mb-0">
								Account Number
							</p>
							<p class="mb-0">
								Expiration Date
							</p>
						</div>
						<div class="card-type">
							VISA
						</div>
						<div class="edit ml-auto">
							<a href="#">
								<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
							</a>
							<a href="#">
								<i class="fa fa-trash" aria-hidden="true"></i>
							</a>
						</div>
					</div>
					<div class="col-md-12 list-wrap mb-40">
						<div class="address">
							<h4>Card Name</h4>
							<p class="mb-0">
								Account Number
							</p>
							<p class="mb-0">
								Expiration Date
							</p>
						</div>
						<div class="card-type">
							VISA
						</div>
						<div class="edit ml-auto">
							<a href="#">
								<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
							</a>
							<a href="#">
								<i class="fa fa-trash" aria-hidden="true"></i>
							</a>
						</div>
					</div>
					<div class="col-md-12 list-wrap mb-40">
						<div class="address">
							<h4>Card Name</h4>
							<p class="mb-0">
								Account Number
							</p>
							<p class="mb-0">
								Expiration Date
							</p>
						</div>
						<div class="card-type">
							VISA
						</div>
						<div class="edit ml-auto">
							<a href="#">
								<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
							</a>
							<a href="#">
								<i class="fa fa-trash" aria-hidden="true"></i>
							</a>
						</div>
					</div>
				</div>
				<div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
					<div class="col-md-12 points-history mb-40">
						<h4 class="mb-40">Current Points : {{$profile->no_of_stamps}} pts <span>Last update : {{$profile->maxdate}}</span>
						</h4>
						<div class="table-responsive">
							@if(isset($stamplist))
								@foreach($stamplist as $key=>$stamps)
								<h2 class="mt-30 mb-30">{{$key}}</h2>
								<table class="table mt-30">
									<thead>
										<tr>
											<th>Points</th>
											<th>Current</th>
											<th>Status</th>
										</tr>
									</thead>
									<tbody>
										@foreach($stamps as $stamp)
											<tr>
												<td>{{$stamp->stamps}}</td>
												<td>{{$stamp->current_stamps}}</td>
												@if($stamp->type == 1)
													<td>ADDED</td>
												@elseif($stamp->type == 2)
													<td>REDEEMED</td>
												@endif
											</tr>
										@endforeach
									</tbody>
								</table>
								@endforeach
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('scripts')
 <script>
    function setDefault(addressId)
	{
		$.ajax({
				method: "POST",
				url: "{{ route('account.address.default') }}",
				data: {
					"_token": "{{ csrf_token() }}",
					"id": addressId,
				},
				success: (response) => {
					$.notify({
						message: 'Default Address Added successfully',
						icon: 'flaticon-shopping-cart'
					},{
						type: 'success',
						allow_dismiss: true,
						placement: {
							from: "bottom",
							align: "right"
						},
					});
					location.reload();
				},
				error: (response) => {
					$.notify("Something went wrong!");
				}
			})
	}
    </script>
@endpush