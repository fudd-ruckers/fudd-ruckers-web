@extends('site.app')
@section('title', 'Homepage')
@section('content')
<div class="main-content-wrap product-listing">
	<div class="menu-wrapper account-edit pb-80">
		<div class="row">
			<form class="col-md-6 m-auto pt-80" id="change_password" name="change_password" method="POST" action="{{route('account.password.update')}}">
			@csrf
			@if ($message = Session::get('success'))
			<div class="alert alert-success alert-block">
				<button type="button" class="close" data-dismiss="alert">×</button>	
					<strong>{{ $message }}</strong>
			</div>
			@endif
			
			@if ($message = Session::get('error'))
			<div class="alert alert-danger alert-block">
				<button type="button" class="close" data-dismiss="alert">×</button>	
					<strong>{{ $message }}</strong>
			</div>
			@endif
				<h1 class="pb-40">Change Password</h1>
				<div class="form-group row">
					<label for="old_password" class="col-sm-4 col-form-label">Old Password</label>
					<div class="col-sm-8">
						<input type="password" class="form-control" id="old_password" name="old_password" placeholder="Password">
					</div>
				</div>
				<div class="form-group row">
					<label for="new_password" class="col-sm-4 col-form-label">New Password</label>
					<div class="col-sm-8">
						<input type="password" class="form-control" id="new_password" name="new_password" placeholder="Password">
					</div>
				</div>
				<div class="form-group row mb-40">
					<label for="confirm_password" class="col-sm-4 col-form-label">Re-type New Password</label>
					<div class="col-sm-8">
						<input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Password">
					</div>
				</div>
				<button type="submit" class="btn btn--primary pl-100 pr-100">Save</button>
			</form>
		</div>
	</div>
</div>
@endsection