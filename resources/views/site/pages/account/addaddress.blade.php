@extends('site.app')
@section('title', 'Homepage')
@section('content')
<div class="main-content-wrap product-listing">
	<div class="menu-wrapper account-edit pb-80">
		<div class="row">
			<form class="col-md-6 m-auto pt-80" name="address_details" id="address_details" method="POST" action="{{route('account.address.store')}}">
				@csrf
				<div class="form-row mb-40">
					<div class="form-group col-md-6">
						<label for="address_alias">Address Name</label>
						<input type="text" class="form-control" id="address_alias" name="address_alias" placeholder="Tap to insert details">
					</div>

				</div>
				<div class="form-row mb-40">
					<div class="form-group col-md-6">
						<label for="flat_number">Flat Number</label>
						<input type="text" class="form-control" id="flat_number" name="flat_number" placeholder="Tap to insert details">
					</div>
					<div class="form-group col-md-6">
						<label for="floor_number">Floor Number</label>
						<input type="text" class="form-control" id="floor_number" name="floor_number" placeholder="Tap to insert details">
					</div>
				</div>
				<div class="form-group mb-40">
					<label for="area">Area</label>
					<select id="area" name="area" class="form-control">
						<option value="English" selected>English</option>
						<option value="Arabic">Arabic</option>
					</select>
				</div>
				<div class="form-row mb-40">
					<div class="form-group col-md-6">
						<label for="block_number">Block Number</label>
						<input type="text" class="form-control" id="block_number" name="block_number" placeholder="Tap to insert details">
					</div>
					<div class="form-group col-md-6">
						<label for="road_number">Road Number</label>
						<input type="text" class="form-control" id="road_number" name="road_number" placeholder="Tap to insert details">
					</div>
				</div>
				<div class="form-group mb-40">
					<label for="additional_information">Additional Information</label>
					<input type="text" class="form-control" id="additional_information" name="additional_information" placeholder="Tap to insert details">
				</div>
				<button type="submit" class="btn btn--primary pl-100 pr-100">Save</button>
			</form>
		</div>
	</div>
</div>
@endsection