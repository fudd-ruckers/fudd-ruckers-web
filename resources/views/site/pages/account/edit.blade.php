@extends('site.app')
@section('title', 'Homepage')
@section('content')
<div class="main-content-wrap product-listing">
	<div class="menu-wrapper account-edit pb-80">
		<div class="row">
			<form class="col-md-6 m-auto pt-80" name="edit" id="edit" method="POST" action="{{route('account.edit.update')}}" enctype="multipart/form-data">
				@csrf
				<div class="avatar-upload">
					<div class="avatar-edit">
						<input type='file' id="imageUpload" name="image" accept=".png, .jpg, .jpeg" />
						<label for="imageUpload"><i class="fa fa-camera" aria-hidden="true"></i>
						</label>
					</div>
					<div class="avatar-preview">
					@if($profile->image!=NULL)
						<div id="imagePreview" style="background-image: url({{ asset('uploads/'.$profile->image)}});">
						</div>
					@else
						<div id="imagePreview" style="background-image: url(http://i.pravatar.cc/500?img=7);">
						</div>
					@endif
					</div>
				</div>
				<div class="form-row mb-40">
					<div class="form-group col-md-6">
						<label for="first_name">First Name</label>
						<input type="text" class="form-control" id="first_name" name="first_name" value="{{$profile->first_name}}" placeholder="John">
					</div>
					<div class="form-group col-md-6">
						<label for="mobile_number">Mobile Number</label>
						<input type="text" class="form-control" id="mobile_number" name="mobile_number" value="{{$profile->mobile_number}}" placeholder="+123 456 789 234">
					</div>
				</div>
				<div class="form-row mb-40">
					<div class="form-group col-md-6">
						<label for="last_name">Last Name</label>
						<input type="text" class="form-control" id="last_name" name="last_name" placeholder="Doe" value="{{$profile->last_name}}">
					</div>
					<div class="form-group col-md-6">
						<label for="email">Email Address</label>
						<input type="email" class="form-control" id="email" name="email" placeholder="john.doe@mail.com" value="{{$profile->email}}">
					</div>
				</div>

				<fieldset class="form-group mb-40">
					<label for="inputState">Gender (Optional)</label>
					<div class="d-flex mt-20">
						<div class="form-check mr-30">
							<input class="form-check-input" type="radio" name="sex" id="sex" value="Male" @if($profile->sex=="Male" || $profile->sex==NULL) checked @endif>
							<label class="form-check-label" for="sex">
								Male
							</label>
						</div>
						<div class="form-check">
							<input class="form-check-input" type="radio" name="sex" id="sex" value="Female" @if($profile->sex=="Female") checked @endif>
							<label class="form-check-label" for="sex">
								Female
							</label>
						</div>
					</div>
				</fieldset>
				<div class="form-group row mb-40">
					<div class="col-sm-12 mb-20">
						<div class="form-check">
							<input class="form-check-input" type="checkbox" id="gridCheck1" name="offers_discounts" @if($profile->offers==1) checked @endif>
							<label class="form-check-label" for="gridCheck1">
								Yes, I want to receive offers and discounts
							</label>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-check">
							<input class="form-check-input" type="checkbox" id="gridCheck2" name="newsletter" @if($profile->newsletter==1) checked @endif>
							<label class="form-check-label" for="gridCheck2">
								Subscribe to Newsletter
							</label>
						</div>
					</div>
				</div>
				<button type="submit" class="btn btn--primary pl-100 pr-100">Save</button>
			</form>
		</div>
	</div>
</div>
@endsection