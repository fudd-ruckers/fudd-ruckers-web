@extends('site.app')
@section('title', 'Menu')
@section('content')
<div class="main-content-wrap product-listing">
<form name="order" id="order" method="post" action="{{route('checkout.place.order')}}">
	@csrf
	<div class="menu-wrapper payment pb-60">
		<div class="row">
			<div class="col-md-8">
				<ul class="nav nav-tabs payment-status mt-60 mb-60" id="myTab" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" id="home-tab" role="tab" aria-controls="home" aria-selected="true">
							<span>1</span>
							<h3>Review Order</h3>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="additional_information-tab" role="tab" aria-controls="additional_information" aria-selected="false">
							<span>2</span>
							<h3>Additional Information</h3>
						</a>
					</li>
				</ul>
				<div class="tab-content" id="myTabContent">
					<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
						@if (!empty($cart['items']))
						@foreach($cart['items'] as $key=>$item)
							<div class="row cart-product mb-30" id="cart_item_{{$key}}">
								<div class="col-5 name-details">
									<strong>{{$item['name']}}</strong>
									@if(!$item['addons']->isEmpty())
										@foreach($item['addons'] as $addon)
											<p class="mb-0 subtitle">{{$addon->name." ".config('settings.currency_code')." ".$addon->price}}</p>
										@endforeach
									@endif
									
									@if(!$item['attributes']->isEmpty())
										@foreach($item['attributes'] as $attribute)
											<p class="mb-0 subtitle">{{$attribute->name." ".config('settings.currency_code')." ".$attribute->price}}</p>
										@endforeach
									@endif
								</div>
								<div class="col-2 pl-0 pr-0">
									<div class="number">
										<span class="minus" onclick="decrementItem({{$key}})">-</span>
										<input type="text" id="item_quantity_{{$key}}" value="{{ $item['quantity'] }}">
										<span class="plus" onclick="incremetItem({{$key}})">+</span>
									</div>
								</div>
								<div class="col-5 text-right">
									<div class="price">
										{{ config('settings.currency_code')." ".$item['price'] }}
									</div>
									<a href="#">
										<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
									</a>
									<a href="javascript:void(0)" onclick="removeFromcart({{$key}})" class="remove">
										<i class="fa fa-trash" aria-hidden="true"></i>
									</a>
								</div>
							</div>
						@endforeach
						<div class="row items">
							<div class="col-md-7">
									<div class="form-group row">
										<label for="redeem_points" class="col-sm-5 col-form-label">REDEEM POINTS ({{$profile->no_of_stamps}}
											pts.)</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" id="redeem_points" name="redeem_points" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..)\./g, '$1');">
											<span style="color:red" id="span_err_points">You don't have sufficient points</span>
										</div>
									</div>
									<div class="form-group row">
										<label for="voucher_code" class="col-sm-5  col-form-label">VOUCHER CODE</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" id="voucher_code" name="voucher_code">
											<span style="color:red" id="span_err_vouchercode">Invalid Voucher Code</span>
										</div>
									</div>
									<a href="{{url('/menu')}}" class="btn btn--primary pl-80 pr-80 mt-30">ADD ITEMS</a>
							</div>
							<div class="col-md-5 subtotal pl-60">
								<div class="row mb-15">
									<div class="col-6">Subtotal</div>
									<div class="col-6 text-right" id="cart_subtotal">{{ $cart['subtotal'] }}</div>
								</div>
								<div class="row mb-15">
									<div class="col-6">Tax</div>
									<div class="col-6 text-right" id="cart_tax">{{ $cart['tax'] }}</div>
								</div>
								<div class="row mb-15">
									<div class="col-6"><strong>Total Amount</strong></div>
									<div class="col-6 text-right" id="cart_payable"><strong>{{ $cart['payable'] }}</strong></div>
								</div>
								<button type="button" id="next" class="btn btn--primary pl-80 pr-80 float-right mt-30 btnNext nexttab">NEXT</button>
							</div>
						</div>
						@endif
					</div>
					<div class="tab-pane fade" id="additional_information" role="tabpanel" aria-labelledby="additional_information-tab">
						<ul class="nav nav-tabs additional-inform" id="myTab" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" id="home-tab" data-toggle="tab" href="#dinein" role="tab" aria-controls="home" aria-selected="true" onclick="changeDelivery('DINE IN');showTableNo();">DINE IN</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="profile-tab" data-toggle="tab" href="#pickup" role="tab" aria-controls="profile" aria-selected="false" onclick="changeDelivery('PICK UP');hideTableNo();">PICK UP</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="contact-tab" data-toggle="tab" href="#delivery" role="tab" aria-controls="contact" aria-selected="false" onclick="changeDelivery('DELIVERY');hideTableNo();">DELIVERY</a>
							</li>
						</ul>
						<div class="tab-content dine-in" id="myTabContent">
							<div class="tab-pane fade show active pt-60 pb-60" id="dinein" role="tabpanel" aria-labelledby="home-tab">
								<h4 class="text-center pb-30">INSERT YOUR<br> TABLE NUMBER</h4>
								<input type="text" class="form-control big-input" placeholder="00" name="table_no" id="table_no" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..)\./g, '$1');">
								<p class="text-center pt-30">ORDER NUMBER:<br>{{$orderNo}}</p>
							</div>
							<div class="tab-pane fade pt-60 pb-60" id="pickup" role="tabpanel" aria-labelledby="profile-tab">
								<h4 class="text-center pb-30">KINDLY PRESENT<br> THIS ORDER NUMBER TO<br> THE
									CASHIER
								</h4>
								<div class="order-no">
									{{$orderNo}}
								</div>
								<input type="hidden" id="order_no" name="order_no" value="{{$orderNo}}">
							</div>
							<div class="tab-pane fade pt-60 pb-60" id="delivery" role="tabpanel" aria-labelledby="contact-tab">
								<div class="row">
									<div class="col-md-6">
										<h6><strong>Deliver To:</strong></h6>
									</div>
									<div class="col-md-6 text-right">
										<input type="hidden" name="address_id" id="address_id" value="@if(isset($profile->address[0]->id)){{$profile->address[0]->id}}@endif">
										<p>{{$profile->first_name." ".$profile->last_name}}</p>
										<p> +973 {{$profile->mobile_number}}</p>
										<p>@if(isset($profile->address[0]->flat_number)){{$profile->address[0]->flat_number}}@endif,</p>
										<p>@if(isset($profile->address[0]->road_number)){{$profile->address[0]->road_number}}@endif,</p>
										<p>@if(isset($profile->address[0]->area)){{$profile->address[0]->area}}@endif,</p>
									</div>
									<div class="col-md-12">
										<h4>Special Instructions?</h4>
										<input type="text" id="special_instruction" name="special_instruction" class="form-control">
									</div>
								</div>
							</div>
							<input type="hidden" id="delivery_type" name="delivery_type" value="DINE IN">
							<div class="row">
								<div class="col-md-12">
									<a href="#" class="btn btn--primary pl-50 pr-50 prevtab">Back</a>
									<a href="#" class="btn btn--primary pl-50 pr-50 float-right">Edit
										Address</a>
								</div>
							</div>
						</div>
					</div>
				</div>


			</div>
			<div class="col-md-4 mt-40 pl-40">
				<div class="cards-wrapper" id="payment">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active pl-0" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">CREDIT CARD</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">BENEFIT</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">CASH</a>
						</li>
					</ul>
					<div class="tab-content" id="myTabContent">
						<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
								<div class="form-group mb-40">
									<label for="card_number">CARD NUMBER</label>
									<input type="text" class="form-control" id="card_number" name="card_number" aria-describedby="emailHelp" placeholder="Enter Card Number" maxlength="16" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..)\./g, '$1');">
								</div>
								<div class="form-row mb-40">
									<div class="form-group col-md-6">
										<label for="expiry_date">EXPIRY DATE</label>
										<input type="text" class="form-control" id="expiry_date" name="expiry_date" placeholder="mm/yy" maxlength="5" onkeyup="return date_slash(this)">
									</div>
									<div class="form-group col-md-6">
										<label for="cvv">CVV</label>
										<input type="text" class="form-control" id="cvv" name="cvv" placeholder="Enter CVV" minlength="3" maxlength="3" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..)\./g, '$1');">
									</div>
								</div>
								<div class="form-group">
									<label for="card_holder_name">CARD HOLDER NAME</label>
									<input type="text" class="form-control" id="card_holder_name" name="card_holder_name" aria-describedby="emailHelp" placeholder="Enter Name" onkeypress="return onlyAlphabets(event,this);">
								</div>
								<button type="submit" class="btn btn--primary mt-80 btn-block">PLACE
									ORDER</button>
						</div>
						<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
							...</div>
						<div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
							...</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
</div>
@endsection
@push('scripts')
<script type="text/javascript" src="{{ asset(asset('frontend/js/jquery.creditCardValidator.js')) }}"></script>
<script>
	$(document).ready(function() {
		$("#span_err_points").hide();
		$("#span_err_vouchercode").hide();
		$('.minus').click(function() {
			var $input = $(this).parent().find('input');
			var count = parseInt($input.val()) - 1;
			count = count < 1 ? 1 : count;
			$input.val(count);
			$input.change();
			return false;
		});
		$('.plus').click(function() {
			var $input = $(this).parent().find('input');
			$input.val(parseInt($input.val()) + 1);
			$input.change();
			return false;
		});
		
		$('.btnNext').click(function() {
                $('.nav-tabs > .active').next('li').find('a').trigger('click');
            });
	});
	
	function incremetItem(index){
			var input = $('#item_quantity_'+index);
			var oldValue = parseFloat(input.val());
			var newVal = oldValue + 1;
			$.ajax({
					method: "POST",
					url: "{{ route('increment.cart') }}",
					data: {
						"_token": "{{ csrf_token() }}",
						"cartItemIndex": index
					},
					success: (response) => {
						$('#total_cart_count').html(response.cartCount);
						$('#cart_subtotal').html(response.subtotal);
						$('#cart_shippingCharges').html(response.shippingCharges);
						$('#cart_tax').html(response.tax);
						$('#cart_payable').html(response.payable);
						
					},
					error: (response) => {
						alert('Something went wrong!');
					}
				})
				
		}
		
	function decrementItem(index){
			var quantity = $('#item_quantity_'+index).val();
			if(quantity == 1)
				$('#cart_item_'+index).remove();
				
			var input = $('#item_quantity_'+index);
			var oldValue = parseFloat(input.val());	
			
			var newVal = oldValue - 1;
				$.ajax({
					method: "POST",
					url: "{{ route('decrement.cart') }}",
					data: {
						"_token": "{{ csrf_token() }}",
						"cartItemIndex": index
					},
					success: (response) => {
						$('#total_cart_count').html(response.cartCount);
						$('#cart_subtotal').html(response.subtotal);
						$('#cart_shippingCharges').html(response.shippingCharges);
						$('#cart_tax').html(response.tax);
						$('#cart_payable').html(response.payable);
						quantity = $('#item_quantity_'+index).val();
						if(quantity == 0)
						$('#cart_item_'+index).remove();
					},
					error: (response) => {
						alert('Something went wrong!');
					}
				})
		}
		
	function removeFromcart(index){
			$.ajax({
				method: "POST",
				url: "{{ route('checkout.cart.remove') }}",
				data: {
					"_token": "{{ csrf_token() }}",
					"cartItemIndex": index
				},
				success: (response) => {
					$('#total_cart_count').html(response.cartCount);
					$('#cart_subtotal').html(response.subtotal);
					$('#cart_shippingCharges').html(response.shippingCharges);
					$('#cart_tax').html(response.tax);
					$('#cart_payable').html(response.payable);
					$('#cart_item_'+index).remove();
					alert('Item removed from the cart successfully');
				},
				error: (response) => {
					alert('Something went wrong!');
				}
			})
		}
		
	function changeDelivery(type)
	{
		$("#delivery_type").val(type);
	}
	
	function hideTableNo()
	{
		$("#table_no").hide();
	}
	
	function showTableNo()
	{
		$("#table_no").show();
	}
</script>
<script type="text/javascript">
        /* -------------------------------------------------------------
              bootstrapTabControl
          ------------------------------------------------------------- */
        function bootstrapTabControl() {
            var i, items = $('.nav-link'),
			pane = $('.tab-pane');
			
			
		// next
			$('.nexttab').on('click', function() {
				var points = $('#redeem_points').val();
				var voucher_code=$('#voucher_code').val();
				var showNext = true;
				$("#span_err_points").hide();
				if(points!='')
				{
					$.ajax({
					method: "POST",
					async: false,
					url: "{{ route('checkout.account.stamp') }}",
					data: {
						"_token": "{{ csrf_token() }}",
						"stamp": points,
					},
					success: (response) => {
						if(response==1)
						{
							showNext = true;
							$("#span_err_points").hide();
						}
						else
						{
							$("#span_err_points").show();
							showNext = false;
							return false;
						}
					},
					error: (response) => {
						alert('Something went wrong!');
					}
					})
				}
				$("#span_err_vouchercode").hide();
				if(voucher_code!=''){
					$.ajax({
						method: "POST",
						async: false,
						url: "{{ route('checkout.account.vouchercode') }}",
						data: {
							"_token": "{{ csrf_token() }}",
							"vouchercode": voucher_code,
						},
						success: (response) => {
							if(response==1)
							{
								$("#span_err_vouchercode").hide();
								showNext = true;
							}
							else
							{
								$("#span_err_vouchercode").show();
								showNext = false;
								return false;
							}
						},
						error: (response) => {
							alert('Something went wrong!');
						}
					})
				}
				if(showNext == true)
				{
						for (i = 0; i < items.length; i++) {
							if ($(items[i]).hasClass('active') == true) {
								break;
							}
						}
						if (i < items.length - 1) {
							// for tab
							$(items[i]).removeClass('active');
							$(items[i + 1]).addClass('active');
							// for pane
							$(pane[i]).removeClass('show active');
							$(pane[i + 1]).addClass('show active');
						}
				}else{
					return false;
				}
				
            });
            // Prev
            $('.prevtab').on('click', function() {
                for (i = 0; i < items.length; i++) {
                    if ($(items[i]).hasClass('active') == true) {
                        break;
                    }
                }
                if (i != 0) {
                    // for tab
                    $(items[i]).removeClass('active');
                    $(items[i - 1]).addClass('active');
                    // for pane
                    $(pane[i]).removeClass('show active');
                    $(pane[i - 1]).addClass('show active');
                }
            });
        }
        bootstrapTabControl();
    </script>
@endpush