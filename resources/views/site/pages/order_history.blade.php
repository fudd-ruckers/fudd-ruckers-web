@extends('site.app')
@section('title', 'Menu')
@section('content')
<div class="main-content-wrap product-listing">
<form name="order" id="order" method="post" action="{{route('checkout.place.order')}}">
	@csrf
	<div class="main-content-wrap product-listing">
            <div class="menu-wrapper order-history pb-80">
                <div class="row">
                    <div class="col-md-12 pt-30">
					@if(isset($orderlist))
						@foreach($orderlist as $key=>$orders)
							<h2 class="mt-30 mb-30">{{$key}}</h2>
							<div id="accordion">
								@foreach($orders as $orderid=>$items)
									<div class="card">
										<div class="card-header" id="heading{{$orderid}}">
											<div class="ord">
											#{{$items[0]->order_id}}
											</div>
											<div class="ven">
												OFFICE
											</div>
											<div class="suc">
												{{$items[0]->status}}
											</div>
											<div class="ml-auto">
												<a href="javascript:void(0)" onclick="reorderTocart({{$orderid}},{{count($items)}})">
													<span class="fa fa-refresh" aria-hidden="true"></span>
												</a>
												<a href="#" data-toggle="collapse" data-target="#collapse{{$orderid}}" aria-expanded="true" aria-controls="collapse{{$orderid}}">
													<span class="fa fa-fw fa-bars"></span>
												</a>
											</div>
										</div>

										<div id="collapse{{$orderid}}" class="collapse {{ $loop->parent->first && $loop->first ? 'show' : '' }}" aria-labelledby="heading{{$orderid}}" data-parent="#accordion">
											<div class="card-body">
												@foreach($items as $item)
													<div class="row cart-product mb-30">
														<div class="col-5 name-details">
															<strong>{{$item->item_name}}</strong>
															<p class="mb-0 pl-2">{{$item->short_text}}</p>
														</div>
														<div class="col-2 pl-0 pr-0">
															<div class="number">
																<span class="minus">-</span>
																<input type="text" id="item_{{$orderid}}_{{ $loop->iteration }}" data-id="{{$item->details_id}}" value="{{$item->quantity}}">
																<span class="plus">+</span>
															</div>
														</div>
														<div class="col-5 text-right">
															<div class="price">
																<strong>{{config('settings.currency_code')." ".$item->quantity * $item->item_price}}</strong>
															</div>
															<a href="#">
																<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
															</a>
															<a href="#">
																<i class="fa fa-trash" aria-hidden="true"></i>
															</a>
														</div>
													</div>
													@endforeach
											</div>
										</div>
									</div>
								@endforeach
							</div>
						@endforeach
					@endif
                    </div>
                </div>
            </div>
        </div>
</form>
</div>
@endsection
@push('scripts')
 <script>
        $(document).ready(function() {
            $('.button-left').click(function() {
                $('.sidebar').toggleClass('fliph');
            });

        });

        $(document).ready(function() {
            $('.minus').click(function() {
                var $input = $(this).parent().find('input');
                var count = parseInt($input.val()) - 1;
                count = count < 1 ? 1 : count;
                $input.val(count);
                $input.change();
                return false;
            });
            $('.plus').click(function() {
                var $input = $(this).parent().find('input');
                $input.val(parseInt($input.val()) + 1);
                $input.change();
                return false;
            });
        });
		
	function reorderTocart(orderId,args)
	{
		var qty = []; var item,value;
		for(var count =1;count<=args;count++){
			item = $("#item_"+orderId+"_"+count).attr('data-id');
			value = $("#item_"+orderId+"_"+count).val();
			qty.push([item, value]);
		}
		$.ajax({
				method: "POST",
				url: "{{ route('reorder.cart') }}",
				data: {
					"_token": "{{ csrf_token() }}",
					"orderId": orderId,
					"qty":qty,
				},
				success: (response) => {
					var cartCount=response.cartCount;
					$('#total_cart_count').html(cartCount);
					$.notify({
						message: 'Reordered successfully',
						icon: 'flaticon-shopping-cart'
					},{
						type: 'success',
						allow_dismiss: true,
						placement: {
							from: "bottom",
							align: "right"
						},
					});
				},
				error: (response) => {
					$.notify("Something went wrong!");
				}
			})
	}
    </script>
@endpush