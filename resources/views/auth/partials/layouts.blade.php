<body style="background: url({{asset('frontend/images/bg.jpg')}}) no-repeat;">
	@include('auth.partials.nav')
    <div class="login">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <img src="{{asset('frontend/images/logo.svg')}}" class="img-fluid logo mb-30" />
                    <h1 class="text-center">Your Place... Your Choice</h1>
                </div>
                @yield('content')
            </div>
        </div>
    </div>
</body>