@if (config('settings.site_favicon') != null)
<link rel="shortcut icon" type="image/x-icon" href="{{ asset('uploads/'.config('settings.site_favicon')) }}">
@else
<link rel="shortcut icon" type="image/x-icon" href="{{ asset('frontend/img/favicon.ico') }}">
@endif
<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/bootstrap.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/style.css') }}"/>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
