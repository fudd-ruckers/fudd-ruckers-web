<nav class="navbar navbar-light bg-light">
	<div class="signup-wrap">
		@if(request()->is('login'))
			<p class="mb-0">Don't have an account?</p>
			<a href="{{ url('/register') }}">
				<button class="btn btn-lg btn--primary btn-block text-uppercase" type="submit">SIGN UP</button>
			</a>
		@else
			<p class="mb-0">Do you have an account?</p>
			<a href="{{ url('/login') }}">
				<button class="btn btn-lg btn--primary btn-block text-uppercase" type="submit">SIGN IN</button>
			</a>
		@endif
		
	</div>
</nav>