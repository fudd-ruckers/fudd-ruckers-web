<script src="{{ asset(asset('frontend/js/jquery.min.js')) }}"></script>
<script src="{{ asset(asset('frontend/js/main.js')) }}"></script>
<script src="{{ asset(asset('frontend/js/scripts.js')) }}"></script>
<script src="{{ asset(asset('frontend/js/validation.js')) }}"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script>
	$(document).ready(function() {
		$('.button-left').click(function() {
			$('.sidebar').toggleClass('fliph');
		});

	});

	$(document).ready(function() {
		$('#search').on("click", (function(e) {
			$(".form-group").addClass("sb-search-open");
			e.stopPropagation()
		}));
		$(document).on("click", function(e) {
			if ($(e.target).is("#search") === false && $(".form-control").val().length == 0) {
				$(".form-group").removeClass("sb-search-open");
			}
		});
		$(".form-control-submit").click(function(e) {
			$(".form-control").each(function() {
				if ($(".form-control").val().length == 0) {
					e.preventDefault();
					$(this).css('border', '2px solid red');
				}
			})
		})
	})
</script>
@stack('scripts')
