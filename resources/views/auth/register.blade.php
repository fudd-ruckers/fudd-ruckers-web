@extends('auth.app')
@section('title', 'Register')
@section('content')
<div class="col-sm-12 col-md-9 col-lg-8 mx-auto">
	<div class="card card-signin mt-30">
		<div class="card-body">
			<h5 class="card-title text-left mb-30">Sign Up</h5>
			<form action="{{ route('register') }}" method="POST" role="form" name="register" id="register">
			@csrf
				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="firstname">First Name</label>
						<input type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" id="first_name" value="{{ old('first_name') }}">
							@error('first_name')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
							@enderror
					</div>
					<div class="form-group col-md-6">
						<label for="lastname">Last Name</label>
						<input type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" id="last_name" value="{{ old('last_name') }}">
							@error('last_name')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
							@enderror
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="email">Email Address</label>
						<input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email" value="{{ old('email') }}">
						@error('email')
						<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>
					<div class="form-group col-md-6">
						<label for="mobile_number">Mobile Number</label>
						<input type="text" class="form-control @error('mobile_number') is-invalid @enderror" name="mobile_number" id="mobile_number"  value="{{ old('mobile_number') }}">
						@error('mobile_number')
						<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="Password">Password</label>
						<input type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="password">
						@error('password')
						<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>
					<div class="form-group col-md-6">
						<label for="rePassword">Re-type Password</label>
						<input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" id="password_confirmation">
						@error('password_confirmation')
						<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>
				</div>
				<div class="form-row mt-30">
					<div class="col-md-6">
						<button class="btn btn-lg btn--primary btn-block text-uppercase mb-20" type="submit">Sign Up</button>
					</div>
					<div class="col-md-6">
						<button class="btn btn-lg btn--secondary btn-block text-uppercase" type="submit"><i class="fa fa-facebook" aria-hidden="true"></i>
							Sign up with Facebook</button>
					</div>
				</div>

			</form>
		</div>
	</div>
</div>
@stop
