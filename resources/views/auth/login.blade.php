@extends('auth.app')
@section('title', 'Login')
@section('content')
	<!-- Start Login Area -->
	<div class="col-sm-12 col-md-9 col-lg-5 mx-auto">
		<div class="card card-signin mt-30">
			<div class="card-body">
				<h5 class="card-title text-left mb-30">Login</h5>
				<form class="form-signin" action="{{ route('login') }}" method="POST" role="form" name="login" id="login">
					@csrf
					<div class="input-group mb-20" id="div_email">
						<span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i>
						</span>
						<input type="email" class="form-control paddding-left @error('email') is-invalid @enderror" name="email" id="email" value="{{ old('email') }}" placeholder="Email">
						@error('email')
						<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>
					<span class="border-danger text-danger"></span>
					<div class="input-group mb-20">
						<span class="input-group-addon"><i class="fa fa-lock" aria-hidden="true"></i>
						</span>
						<input type="password" class="form-control paddding-left @error('password') is-invalid @enderror" name="password" id="password" placeholder="Password">
						@error('password')
						<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>

					<div class="custom-control pl-0 text-center">
						<a href="{{route('password.request')}}" class="forgot">Forgot password</a>
					</div>
					<button class="btn btn-lg btn--primary btn-block text-uppercase mt-80 mb-3" type="submit">LOGIN</button>
					
					<a href="{{ url('auth/facebook') }}" class="btn btn-lg btn--secondary btn-block text-uppercase"><i class="fa fa-facebook" aria-hidden="true"></i>
						Sign in with Facebook
					</a> 
				</form>
			</div>
		</div>
	</div>
	<!-- End Login Area -->
@stop
