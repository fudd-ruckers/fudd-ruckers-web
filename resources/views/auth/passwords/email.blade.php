@extends('auth.app')
@section('title', 'Register')
@section('content')
<div class="col-sm-12 col-md-9 col-lg-5 mx-auto">
	<div class="card card-signin mt-30">
		<div class="card-body">
			<h5 class="card-title text-center mt-40">Forgot Password?</h5>
			<p class="mb-40">Enter email address for account recovery.</p>
			<form action="{{route('password.email')}}" method="post" name="login" id="login">
			{{csrf_field()}}
				<div class="input-group mb-20">
					<input type="email" class="form-control form-control-danger" placeholder="Enter email" name="email">
				</div>
				<button class="btn btn-lg btn--primary btn-block text-uppercase mb-60" type="submit">Next</button>
			</form>
		</div>
	</div>
</div>
@stop
