@extends('auth.app')
@section('title', 'Register')
@section('content')
	<section class="login-area ptb-100">
		<div class="container">
			<div class="login-form">
				<h2>Reset Password</h2>
				@if (session('status'))
				   <p class="alert alert-success">{{ session('status') }}</p>
				@endif
				<form action="{{route('password.update')}}" method="post" name="register" id="register">
					{{csrf_field()}}
					<input type="hidden" name="email" value="praseetha@testing.com">
					<input type="hidden" name="token" value="{{ $token }}">
					<div class="form-group">
						<input type="password" class="form-control form-control-danger" placeholder="Enter password" name="password" id="password">
					</div>
					<div class="form-group">
						<input type="password" class="form-control form-control-danger" placeholder="Enter Confirm password" name="password_confirmation" id="password_confirmation">
					</div>
					<button type="submit" class="default-btn">Reset Password</button>
				</form>
			</div>
		</div>
	</section>
@stop
