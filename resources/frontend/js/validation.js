$(document).ready(function() {
	/// Register Form Validation ///

	$("#register").validate({
		errorElement: 'span', // default input error message container
		errorClass: 'border-danger text-danger', // default input error message class
		rules: {
			first_name:{
					required: true,
				},
				last_name:{
					required: true,
				},
				email:{
					required: true,
				},
				password:{
					required: true,
				},
				password_confirmation: {
				  equalTo: '#password',
				  required: true,
				},
				mobile_number:{
					required: true,
				},
		},
	});
	
	/// /Register Form Validation ///
	
	/// Login Form Validation ///
	$("#login").validate({
		errorElement: 'span', // default input error message container
		errorClass: 'border-danger text-danger', // default input error message class
		rules: {
			email:{
					required: true,
				},
			password:{
					required: true,
				},
		},
	});
	/// /Login Form Validation ///
	
	/// Add address Form Validation ///
	$("#address_details").validate({
		errorElement: 'span', // default input error message container
		errorClass: 'border-danger text-danger', // default input error message class
		rules: {
			address_alias:{
					required: true,
				},
			flat_number:{
					required: true,
				},
			floor_number:{
					required: true,
				},
			area:{
					required: true,
				},
			block_number:{
					required: true,
				},
			road_number:{
					required: true,
				},
			additional_information:{
					required: true,
				},
		},
	});
	/// /Add address Form Validation ///
	
	/// Change Password Form Validation ///
	$("#change_password").validate({
		errorElement: 'span', // default input error message container
		errorClass: 'border-danger text-danger', // default input error message class
		rules: {
			old_password:{
					required: true,
				},
			new_password:{
					required: true,
				},
			confirm_password: {
				  equalTo: '#new_password',
				  required: true,
				},
		},
	});
	/// /Change Password Form Validation ///
	
	/// Order Form Validation ///
	$("#order").validate({
		errorElement: 'span', // default input error message container
		errorClass: 'border-danger text-danger', // default input error message class
		rules: {
			table_no:{required: true,minlength: 1,number:true,},
			card_number:{required:true,number:true, minlength: 16, credits:true },
			expiry_date: { required: true, ccexpdate: true },
			cvv:{required:true,normalizer: function(value) { return $.trim(value);  },number:true,maxlength:3 },
			card_holder_name:{required: true,},
		},
		messages: {
			table_no: {
			  required: "Enter Table Number",
			  minlength: jQuery.validator.format("At least {0} characters required!"),
			  number:"Enter Valid Number",
			}
		  }
	});
	/// /Order Form Validation ///
	
	$.validator.addMethod("credits", function(value, element)
    {   var valid;
        $('#card_number').validateCreditCard(function(result){
            if(!(result.valid)){ valid = false; }
            else { valid = true; }
        }); 
        if(valid == true)  { return true }else{ return false } 
   }, "Please enter a  valid credit card number."); 
});

/* ------------------------------------------------------------------------- */ 
/* ------------------------------ date slash format ------------------------ */ 
/* ------------------------------------------------------------------------- */ 
    
function date_slash(element)
{  
    var value =element.value;
    var returns = value.replace(/^(\d\d)(\d)$/g,'$1/$2').replace(/^(\d\d\/\d\d)(\d+)$/g,'$1/$2').replace(/[^\d\/]/g,'');  
    document.getElementById(element.id).value =returns; 
}
    
/* ------------------------------------------------------------------------- */ 
/* ----------------------------- /date slash format ------------------------ */ 
/* ------------------------------------------------------------------------- */

/* ------------------------------------------------------------------------- */ 
/* --------------------------------- only Alphabets ------------------------ */ 
/* ------------------------------------------------------------------------- */ 
     
function onlyAlphabets(event) 
{ 
    var inputValue = event.charCode;
    if(!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0))
    {
        event.preventDefault();
    }  
}
    
/* ------------------------------------------------------------------------- */ 
/* --------------------------------- only Alphabets ------------------------ */ 
/* ------------------------------------------------------------------------- */ 

//expire date validate
    
    $.validator.addMethod("ccexpdate",function (value, element) 
    { 
        // Initialize todays date   i.e start date
        var today = new Date();
        var startDate = new Date(today.getFullYear(),today.getMonth(),1,0,0,0,0);

        // Initialize End/Expiry date i.e. adding 10 years to expire
        var futureLimitDate= new Date(today.getFullYear()+10,today.getMonth(),1,0,0,0,0);
        var expDate = value; 
        var expYearCheck=''; 
        // Check Date format
        var separatorIndex = expDate.indexOf('/');
        if(separatorIndex==-1)return false; // Return false if no / found

        var expDateArr=expDate.split('/'); 
        if(expDateArr.length>2)return false; // Return false if no num/num format found

        // Check Month for validity
        if(eval(expDateArr[0])<1||eval(expDateArr[0])>12)
        {
            //If month is not valid i.e not in range 1-12
            return false;
        } 
        //Check Year for format YY or YYYY
        switch(expDateArr[1].length)
        {
            case 2:expYearCheck=2000+parseInt(expDateArr[1], 10);break; // If YY format convert it to 20YY to it
            case 4:expYearCheck=expDateArr[1];break; // If YYYY format assign it to check Year Var
            default:return false;break;
        } 
        // Calculated new exp Date for ja  
        expDate=new Date(eval(expYearCheck),(eval(expDateArr[0])-1),1,0,0,0,0); 
        if(Date.parse(startDate) <= Date.parse(expDate))
        {
            if(Date.parse(expDate) <= Date.parse(futureLimitDate)) {  /*  Date validated */  return true; }else { /*  Date exceeds future date */ return false; }

        }else {  /* Date is earlier than todays date */ return false; } 

    }, "Must be a valid Expiration Date." ); 
// end expire date validate