const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.copyDirectory('resources/backend', 'public/backend');
mix.copyDirectory('resources/frontend', 'public/frontend');

mix.js('resources/js/app.js', 'public/backend/js');
mix.js('resources/js/site.js', 'public/frontend/js');

// mix.js('resources/js/app.js', 'public/js')
//    .sass('resources/sass/app.scss', 'public/css');


/*
|--------------------------------------------------------------------------
| Front
|--------------------------------------------------------------------------
*/
var jsPath = 'resources/frontend/js/'
mix.combine([
  // ** Required Plugins **
  jsPath + 'jquery.min.js',
  jsPath + 'bootstrap.min.js',
  jsPath + 'jquery.magnific-popup.min.js',
  jsPath + 'form-validator.min.js',
  jsPath + 'main.js',
  jsPath + 'scripts.js',
  jsPath + 'validation-script.js',
], 'public/frontend/js/plugins.js')


.options({
    processCssUrls: false
})

.version();

var cssPath = 'resources/frontend/css/'
mix.styles([
    cssPath + 'bootstrap.min.css',
    cssPath + 'animate.min.css',
    cssPath + 'meanmenu.css',
    cssPath + 'boxicons.min.css',
    cssPath + 'flaticon.css',
    cssPath + 'nice-select.min.css',
    cssPath + 'owl.carousel.min.css',
    cssPath + 'owl.theme.default.min.css',
    cssPath + 'odometer.min.css',
    cssPath + 'magnific-popup.min.css',
    cssPath + 'style.css',
    cssPath + 'jquery.raty.css',
    cssPath + 'responsive.css',
], 'public/frontend/css/plugins.css')
.options({
    processCssUrls: false
})
.version();