<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('auth/facebook', 'Auth\FacebookController@redirectToFacebook');
Route::get('auth/facebook/callback', 'Auth\FacebookController@handleFacebookCallback');

Route::get('/', 'Site\HomeController@index');
Route::get('/home', 'Site\HomeController@index');
Route::get('/menu', 'Site\MenuController@index');
Route::get('/contact', 'Site\HomeController@contact');
Route::get('/cart', 'Site\CartController@getCart');

Route::post('/product/add/cart', 'Site\MenuController@addToCart')->name('product.add.cart');
Route::get('/cart-json', 'Site\CartController@getCartJson');
Route::post('/cart/item/remove', 'Site\CartController@removeItem')->name('checkout.cart.remove');
Route::get('/cart/clear', 'Site\CartController@clearCart')->name('checkout.cart.clear');
Route::post('add-to-cart', 'Site\CartController@addToCart')->name('add.cart');
Route::post('increment-cart-item', 'Site\CartController@incrementCartItem')->name('increment.cart');
Route::post('decrement-cart-item', 'Site\CartController@decrementCartItem')->name('decrement.cart');

Route::post('reorder-to-cart',  'Site\CartController@reorderToCart')->name('reorder.cart');

Route::group(['middleware' => ['auth']], function () {
	Route::get('/', 'Site\HomeController@index');
	Route::get('/history', 'Site\HistoryController@index');
	Route::get('payment-razorpay', 'Site\PaymentController@create')->name('paywithrazorpay');

	Route::post('payment', 'Site\PaymentController@payment')->name('payment');
	
	Route::post('/checkout/check-stamp', 'Site\CheckoutController@checkStamp')->name('checkout.account.stamp');
	Route::post('/checkout/voucher-code', 'Site\CheckoutController@checkVoucherCode')->name('checkout.account.vouchercode');
    Route::post('/checkout/order', 'Site\CheckoutController@placeOrder')->name('checkout.place.order');

    Route::get('checkout/payment/complete', 'Site\CheckoutController@complete')->name('checkout.payment.complete');

    Route::get('account/orders', 'Site\AccountController@getOrders')->name('account.orders');
	Route::get('account/orders/{orderNo}/view', 'Site\AccountController@viewOrder')->name('account.orders.view');
	Route::get('account/profile', 'Site\AccountController@getProfile')->name('account.profile');
	Route::get('account/address', 'Site\AccountController@addAddress')->name('account.address');
	Route::post('account/address/store', 'Site\AccountController@storeAddress')->name('account.address.store');
	Route::post('account/address/default', 'Site\AccountController@defaultAddress')->name('account.address.default');
	Route::get('account/edit', 'Site\AccountController@editProfile')->name('account.edit');
	Route::post('account/edit/update', 'Site\AccountController@updateProfile')->name('account.edit.update');
	Route::get('account/change-password', 'Site\AccountController@changePassword')->name('account.password');
	Route::post('account/change-password/update', 'Site\AccountController@updatePassword')->name('account.password.update');
	Route::get('account/address/delete/{id}', 'Site\AccountController@deleteAddress')->name('account.address.delete');
	
	Route::get('account/address/edit/{id}',  'Site\AccountController@editAddress')->name('account.address.edit');
	Route::post('account/address/update',  'Site\AccountController@updateAddress')->name('account.address.update');
	Route::get('brand/register', 'Site\BrandController@index')->name('brand.register');
	Route::post('brand/store', 'Site\BrandController@store')->name('brand.store');
	Route::get('brand/products', 'Site\BrandController@getProducts')->name('brand.products');
	Route::get('brand/products/create', 'Site\BrandController@create_product')->name('brand.products.create');
	Route::post('brand/products/store', 'Site\BrandController@store_product')->name('brand.products.store_product');
	Route::get('brand/products/{id}/edit', 'Site\BrandController@edit_product')->name('brand.products.edit');
	Route::post('brand/products/image/upload', 'Site\BrandController@image_upload')->name('brand.products.image.upload');
	Route::get('brand/products/image/{id}/delete', 'Site\BrandController@image_delete')->name('brand.products.image.delete');
	Route::post('brand/products/update', 'Site\BrandController@update_product')->name('brand.products.update_product');
	
	Route::get('order/success/{orderId}', 'Site\CheckoutController@orderSucess')->name('order.success');
	
	Route::post('product/review/post', 'Site\ProductController@postReview')->name('product.review.post');
});



Auth::routes();
require 'admin.php';
