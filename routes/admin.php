<?php

Route::group(['prefix'  =>  'admin'], function () {

    Route::get('login', 'Admin\LoginController@showLoginForm')->name('admin.login');
    Route::post('login', 'Admin\LoginController@login')->name('admin.login.post');
    Route::get('logout', 'Admin\LoginController@logout')->name('admin.logout');

    Route::group(['middleware' => ['auth:admin']], function () {

        Route::get('/', function () {
            return view('admin.dashboard.index');
        })->name('admin.dashboard');

        Route::get('/settings', 'Admin\SettingController@index')->name('admin.settings');
        Route::post('/settings', 'Admin\SettingController@update')->name('admin.settings.update');

        Route::group(['prefix'  =>   'categories'], function() {

            Route::get('/', 'Admin\CategoryController@index')->name('admin.categories.index');
            Route::get('/create', 'Admin\CategoryController@create')->name('admin.categories.create');
            Route::post('/store', 'Admin\CategoryController@store')->name('admin.categories.store');
            Route::get('/{id}/edit', 'Admin\CategoryController@edit')->name('admin.categories.edit');
            Route::post('/update', 'Admin\CategoryController@update')->name('admin.categories.update');
            Route::get('/{id}/delete', 'Admin\CategoryController@delete')->name('admin.categories.delete');

        });

        Route::group(['prefix'  =>   'attributes'], function() {

            Route::get('/', 'Admin\AttributeController@index')->name('admin.attributes.index');
            Route::get('/create', 'Admin\AttributeController@create')->name('admin.attributes.create');
            Route::post('/store', 'Admin\AttributeController@store')->name('admin.attributes.store');
            Route::get('/{id}/edit', 'Admin\AttributeController@edit')->name('admin.attributes.edit');
            Route::post('/update', 'Admin\AttributeController@update')->name('admin.attributes.update');
            Route::get('/{id}/delete', 'Admin\AttributeController@delete')->name('admin.attributes.delete');

            Route::post('/get-values', 'Admin\AttributeValueController@getValues');
            Route::post('/add-values', 'Admin\AttributeValueController@addValues');
            Route::post('/update-values', 'Admin\AttributeValueController@updateValues');
            Route::post('/delete-values', 'Admin\AttributeValueController@deleteValues');
        });

        Route::group(['prefix'  =>   'brands'], function() {

            Route::get('/', 'Admin\BrandController@index')->name('admin.brands.index');
            Route::get('/create', 'Admin\BrandController@create')->name('admin.brands.create');
            Route::post('/store', 'Admin\BrandController@store')->name('admin.brands.store');
            Route::get('/{id}/edit', 'Admin\BrandController@edit')->name('admin.brands.edit');
            Route::post('/update', 'Admin\BrandController@update')->name('admin.brands.update');
            Route::get('/{id}/delete', 'Admin\BrandController@delete')->name('admin.brands.delete');

        });

        Route::group(['prefix' => 'menu'], function () {

           Route::get('/', 'Admin\MenuController@index')->name('admin.menu.index');
           Route::get('/create', 'Admin\MenuController@create')->name('admin.menu.create');
           Route::post('/store', 'Admin\MenuController@store')->name('admin.menu.store');
           Route::get('/edit/{id}', 'Admin\MenuController@edit')->name('admin.menu.edit');
           Route::post('/update', 'Admin\MenuController@update')->name('admin.menu.update');

           Route::post('images/upload', 'Admin\MenuMediaController@upload')->name('admin.menu.images.upload');
           Route::get('images/{id}/delete', 'Admin\MenuMediaController@delete')->name('admin.menu.images.delete');
		   Route::post('media/upload', 'Admin\MenuMediaController@mediaUpload')->name('admin.menu.media.upload');

           Route::get('attributes/load', 'Admin\ProductAttributeController@loadAttributes');
           Route::post('attributes', 'Admin\ProductAttributeController@productAttributes');
           Route::post('attributes/values', 'Admin\ProductAttributeController@loadValues');
           Route::post('attributes/add', 'Admin\ProductAttributeController@addAttribute');
           Route::post('attributes/delete', 'Admin\ProductAttributeController@deleteAttribute');

        });

        Route::group(['prefix' => 'orders'], function () {
           Route::get('/', 'Admin\OrderController@index')->name('admin.orders.index');
           Route::get('/{order}/show', 'Admin\OrderController@show')->name('admin.orders.show');
        });
		
		Route::group(['prefix' => 'customers'], function () {
           Route::get('/', 'Admin\CustomerController@index')->name('admin.customers.index');
           //Route::get('/{order}/show', 'Admin\OrderController@show')->name('admin.orders.show');
		   Route::get('/{id}/edit', 'Admin\CustomerController@edit')->name('admin.customers.edit');
		   Route::post('/update', 'Admin\CustomerController@update')->name('admin.customers.update');
		   Route::get('/{id}/destroy', 'Admin\CustomerController@destroy')->name('admin.customers.destroy');
		   Route::post('/activate', 'Admin\CustomerController@activate_user')->name('admin.customers.activate');
        });
		
		Route::group(['prefix' => 'voucher'], function () {
			Route::get('/', 'Admin\VoucherController@index')->name('admin.voucher.index');
			Route::get('/create', 'Admin\VoucherController@create')->name('admin.voucher.create');
			Route::post('/store', 'Admin\VoucherController@store')->name('admin.voucher.store');
			Route::get('/{id}/edit', 'Admin\VoucherController@edit')->name('admin.voucher.edit');
			Route::post('/update', 'Admin\VoucherController@update')->name('admin.voucher.update');
			Route::get('/{id}/delete', 'Admin\VoucherController@delete')->name('admin.voucher.delete');
		});
		
		Route::group(['prefix' => 'offer'], function () {
			Route::get('/', 'Admin\OfferController@index')->name('admin.offer.index');
			Route::get('/create', 'Admin\OfferController@create')->name('admin.offer.create');
			Route::post('/store', 'Admin\OfferController@store')->name('admin.offer.store');
			Route::get('/{id}/edit', 'Admin\OfferController@edit')->name('admin.offer.edit');
			Route::post('/update', 'Admin\OfferController@update')->name('admin.offer.update');
			Route::get('/{id}/delete', 'Admin\OfferController@delete')->name('admin.offer.delete');
			Route::post('/storeMedia', 'Admin\OfferController@storeMedia')->name('admin.offer.storeMedia');
		});
		
		Route::get('/general', 'Admin\GeneralContentController@index')->name('admin.general');
		Route::post('/general', 'Admin\GeneralContentController@update')->name('admin.general.update');
    });
});
