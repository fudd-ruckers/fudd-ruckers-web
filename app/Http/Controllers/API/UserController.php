<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Validator;

/* use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response; */

class UserController extends Controller
{

	public $successStatus = 200;
	
	public function login(){
		
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user(); 
            $response['token'] =  $user->createToken('Freshoppe')->accessToken; 
            return response()->json(['response' => $response], $this->successStatus); 
        } 
        else{ 
            return response()->json(['error'=>'Unauthorised'], 401); 
        } 
    }
	
	
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function register(Request $request)
    {
		
		
		$validator = Validator::make($request->all(), [ 
			'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
			'password_confirmation' => 'required|same:password',
       
		]);
		if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
		
		$input = $request->all();
		$input['password'] = bcrypt($input['password']); 
		$user = User::create($input);
		$response['token'] =  $user->createToken('Freshoppe')->accessToken;
		$response['user'] =  $user;
		return response()->json(['response'=>$response], $this->successStatus);
    }
	
	public function details() 
    { 
        $user = Auth::user(); 
        return response()->json(['response' => $user], $this->successStatus); 
    } 
	
	
}
