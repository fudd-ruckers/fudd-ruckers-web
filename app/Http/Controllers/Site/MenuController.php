<?php

namespace App\Http\Controllers\Site;

//use Cart;
use Illuminate\Http\Request;
use App\Contracts\MenuContract;
use App\Http\Controllers\Controller;
use App\Contracts\AttributeContract;
use App\Contracts\CategoryContract;
use App\Models\Menu;
use App\Models\Category;
use App\Models\ProductReview;

class MenuController extends Controller
{
    protected $productRepository;

    protected $attributeRepository;

    public function __construct(MenuContract $productRepository, AttributeContract $attributeRepository, CategoryContract $categoryRepository)
    {
        $this->productRepository = $productRepository;
        $this->attributeRepository = $attributeRepository;
		$this->categoryRepository = $categoryRepository;
    }

	public function index(Request $request)
    {
        $products = $this->productRepository->getAllProducts();
		$category = $this->categoryRepository->getProductCategory();
		$attributes = $this->attributeRepository->listAttributes();
		/* $response = [
           'pagination' => [
               'total' => $products->total(),
               'per_page' => $products->perPage(),
               'current_page' => $products->currentPage(),
               'last_page' => $products->lastPage(),
               'from' => $products->firstItem(),
               'to' => $products->lastItem()
           ],
           'data' => $products
       ]; */
       //return response()->json($response);
		
		return view('site.pages.menu',['category' => $category,'attributes' => $attributes]);
    }
	
	public function getSortedProducts(Request $request)
    {
        $products = $this->productRepository->getShopProducts($request->sort);
        return response()->json([
            'products'=>$products
        ]);
         
    }
	
    public function show($slug)
    {
        $product 			= $this->productRepository->findProductBySlug($slug);
		//$catIds				= $product->categories->pluck('id')->toArray();
        $relatedProducts 	= $this->productRepository->getRelatedProducts($product->category_id,$product->id);
		$featuredProducts 	= $this->productRepository->getFeaturedProducts($product->id);
        $attributes 		= $this->attributeRepository->listAttributes();
        return view('site.pages.product', compact('product', 'attributes','relatedProducts','featuredProducts'));
    }

    public function addToCart(Request $request)
    {
        $product = $this->productRepository->findProductById($request->input('productId'));
        /* $options = $request->except('_token', 'productId', 'price', 'qty');

        Cart::add(uniqid(), $product->name, $request->input('price'), $request->input('qty'), $options); */

		Menu::addToCart($product->id,$request->input('qty'),$request->input('attribute_id'));
		//dd($request);
		
        return redirect()->back()->with('message', 'Item added to cart successfully.');
    }
	
	public function postReview(Request $request)
	{
		$review = new ProductReview();
		$review->product_id = $request->product_id;
		$review->name 		= $request->name;
		$review->rating 	= $request->score;
		$review->email 		= $request->email;
		$review->title 		= $request->title;
		$review->comments 	= $request->comments;
		$review->verified 	= 1;
		$review->save();
		return redirect()->back()->with('message', 'Thanks for your review');
	}
}


