<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Menu;
use App\Models\Order;
use App\Models\CartSubItem;
use App\Models\CartItemAttribute;
use App\Contracts\OrderContract;
use App\Contracts\AttributeContract;
use App\Contracts\CartSubItemContract;
use App\Contracts\CartAttributeContract;

class CartController extends Controller
{
	protected $attributeRepository;
	protected $cartItemRepository;
	protected $cartAttributeRepository;

    public function __construct(AttributeContract $attributeRepository,CartSubItemContract $cartItemRepository,CartAttributeContract $cartAttributeRepository,OrderContract $orderRepository)
    {
        $this->attributeRepository = $attributeRepository;
		$this->cartItemRepository = $cartItemRepository;
		$this->cartAttributeRepository = $cartAttributeRepository;
		$this->orderRepository = $orderRepository;
    }
	
    public function getCart()
    {
		$cart = cart()->toArray();
		if (!empty($cart['items'])){
			foreach($cart['items'] as $key=>$item){
				$product = Menu::with('images')->with('attributes')->where('id', $item['modelId'])->first();
				$addons = CartSubItem::join('menus','menus.id','=','cart_sub_items.menu_id')
								->where('cart_sub_items.cart_id',cart()->id())
								->where('cart_sub_items.cart_item_id',$item['id'])->get();
				$attributes = CartItemAttribute::join('menu_attributes','menu_attributes.id','=','cart_item_attributes.attribute_value_id')
								->join('attributes','attributes.id','=','menu_attributes.attribute_id')
								->where('cart_item_attributes.cart_id',cart()->id())
								->where('cart_item_attributes.cart_item_id',$item['id'])->get();
				if(isset($product->images[0]->media_file)):
					$cart['items'][$key]['image'] 	= $product->images[0]->media_file;
				endif;
				$cart['items'][$key]['short_text'] 	= $product->short_text;
				$cart['items'][$key]['slug'] 	= $product->slug;
				$cart['items'][$key]['qty'] 	= $product->quantity;
				$cart['items'][$key]['attr'] 	= $product->attributes;
				$cart['items'][$key]['addons'] 	= $addons;
				$cart['items'][$key]['attributes'] 	= $attributes;
			}
		}
		$attributes 		= $this->attributeRepository->listAttributes();
		$profile = auth()->user();
		$orderNo=mt_rand(10000, 99999);
        return view('site.pages.myorder',compact('cart','attributes','profile','orderNo'));
    }

    public function removeItem()
    {
		cart()->removeAt(request('cartItemIndex'));
		return response()->json([
            'message' 			=> 'Item removed from the cart successfully',
			'cartCount' 		=> count(cart()->items()),
			'subtotal'  		=> cart()->getSubtotal(),
			'shippingCharges' 	=> cart()->shippingCharges(),
			'tax' 				=> cart()->tax(),
			'payable' 			=> cart()->payable(),
        ], 200);
    }

    public function clearCart()
    {
		cart()->clear();
        return redirect('/');
    }
	
	/**
     * Add to cart.
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function addToCart()
    {
		$addonPrice=0;$drinkPrice=0;$totalPrice=0;$attributePrice=0;
		
        $menu=Menu::addToCart(request('menuId'),request('qty'));
		$email_update=\DB::table('carts')
					->where('id', cart()->id())
					->limit(1)
					->update(array('special_request' => request('special_request')));
		if(request('attributes')!=NULL):
			$clear_attributes=$this->cartAttributeRepository->clearAttribute(request('menuId'));
			$menu_attribute=$this->cartAttributeRepository->createCartAttributes(request('attributes'),request('menuId'));
			$attributePrice=$this->cartAttributeRepository->getAttributePrice(request('attributes'));
		endif;
		
		if(request('addons')!=NULL || request('drinks')!=NULL):
			$clear_sub_items=$this->cartItemRepository->clearSubItem(request('menuId'));
		endif;
		
		if(request('addons')!=NULL):
			$menu_addons=$this->cartItemRepository->createCartAddons(request('addons'),request('menuId'));
			$addonPrice=$this->cartItemRepository->getAddonPrice(request('addons'));
		endif;
		
		if(request('drinks')!=NULL):
			$menu_drinks=$this->cartItemRepository->createCartDrinks(request('drinks'),request('menuId'));
			$drinkPrice=$this->cartItemRepository->getDrinkPrice(request('drinks'));
		endif;
		
		$totalPrice=$totalPrice+cart()->netTotal()+$addonPrice+$drinkPrice+$attributePrice;
		
		$updateNetTotal=$this->cartItemRepository->updateNetTotal($totalPrice);
		
		$updateTotal=$this->cartItemRepository->updateTotal($totalPrice);
		
		$updatePayable=$this->cartItemRepository->updatePayable($totalPrice);
		
        return response()->json([
            'message' => 'Item added to Cart Successfully',
			'cartCount' => count(cart()->items())
        ], 200);
    }
	
	/**
     * Reorder to cart
     * @return Illuminate\Http\JsonResponse
     */
	 
	public function reorderToCart()
    {
		$orderId = request('orderId');
		$qtys = request('qty');
		
		foreach($qtys as $qty):
			$item_qty[$qty[0]] = $qty[1];
		endforeach;
		
		$items=$this->orderRepository->itemsByOrder($orderId);
		foreach($items as $item){
			$menu=Menu::addToCart($item->menu_id,$item_qty[$item->id]);
			$update=\DB::table('carts')
					->where('id', cart()->id())
					->limit(1);
		}
        
        return response()->json([
            'message' => 'Item added to Cart Successfully',
			'cartCount' => count(cart()->items()),
        ], 200);
    }
	
	/**
     * Increment cart item quantity.
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function incrementCartItem()
    {
        cart()->incrementQuantityAt(request('cartItemIndex'));

        return response()->json([
            'message' 			=> 'Item added to the cart successfully',
			'cartCount' 		=> count(cart()->items()),
			'subtotal'  		=> cart()->getSubtotal(),
			'shippingCharges' 	=> cart()->shippingCharges(),
			'tax' 				=> cart()->tax(),
			'payable' 			=> cart()->payable(),
        ], 200);
    }
	
	/**
     * Decrement cart item quantity.
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function decrementCartItem()
    {
        cart()->decrementQuantityAt(request('cartItemIndex'));

        return response()->json([
            'message' 			=> 'Item reoved from cart successfully',
			'cartCount' 		=> count(cart()->items()),
			'subtotal'  		=> cart()->getSubtotal(),
			'shippingCharges' 	=> cart()->shippingCharges(),
			'tax' 				=> cart()->tax(),
			'payable' 			=> cart()->payable(),
        ], 200);
    }
	
	public function getCartJson()
    {
		$cart = cart()->toArray();
		if (!empty($cart['items'])){
			foreach($cart['items'] as $key=>$item){
				$product = Menu::with('images')->with('attributes')->where('id', $item['modelId'])->first();
				if(isset($product->images[0]->media_file)):
					$cart['items'][$key]['image'] 	= $product->images[0]->media_file;
				endif;
				$cart['items'][$key]['slug'] 	= $product->slug;
				$cart['items'][$key]['qty'] 	= $product->quantity;
				$cart['items'][$key]['attr'] 	= $product->attributes;
				$cart['items'][$key]['short_text'] 	= $product->short_text;
			}
		}
		$attributes 		= $this->attributeRepository->listAttributes();
		$cartArray['cart']			=	$cart;
		return response()->json([
            'message' 		=> 'cart listed successfully',
			'data' 			=> $cartArray,
        ], 200);
		
    }
}
