<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Contracts\MenuContract;
use App\Contracts\CategoryContract;
use App\Repositories\GeneralContentRepository;

class HomeController extends Controller
{
	protected $productRepository;
	protected $categoryRepository;
	
	public function __construct(MenuContract $productRepository,CategoryContract $categoryRepository,GeneralContentRepository $generalRepository)
    {
        $this->productRepository = $productRepository;
		$this->categoryRepository = $categoryRepository;
		$this->generalRepository = $generalRepository;
    }
	
	public function index()
	{
		//$product = $this->productRepository->getProducts();
		$product = $this->productRepository->getProducts();
		$category = $this->categoryRepository->getCategory();
		return view('site.pages.homepage',compact('category','product'));
	}
	
	public function contact()
	{
		return view('site.pages.contact');
	}
	
	public function contactSubmit(Request $request)
	{
		$contact=new Contact();
		$contact->first_name = $request->first_name;
		$contact->last_name = $request->last_name;
		$contact->email = $request->email;
		$contact->phone_number = $request->phone_number;
		$contact->msg_subject = $request->msg_subject;
		$contact->message = $request->message;
		if($contact->save()):
		echo json_encode("Thanks for contacting us! We will be in touch with you shortly.");
		else:
		echo json_encode("Error! Please Try Again Later");
		endif;
	}
	
	public function terms()
	{
		$general = $this->generalRepository->getContent();
		return view('site.pages.terms_and_conditions',compact('general'));
	}
	
	public function privacy()
	{
		$general = $this->generalRepository->getContent();
		return view('site.pages.privacy_policy',compact('general'));
	}
}
