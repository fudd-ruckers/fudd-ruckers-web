<?php

namespace App\Http\Controllers\Site;

//use Cart;
use App\Models\Order;
use App\Models\Product;
use App\Models\AddressDetails;
use App\Models\OrderDetails;
use Illuminate\Http\Request;
use App\Services\PayPalService;
use App\Contracts\OrderContract;
use App\Contracts\StampContract;
use App\Http\Controllers\BaseController;
use App\Http\Requests\StoreCheckoutFormRequest;

class CheckoutController extends BaseController
{
    protected $payPal;

    protected $orderRepository;
	protected $stampRepository;

    public function __construct(OrderContract $orderRepository, StampContract $stampRepository, PayPalService $payPal)
    {
        $this->payPal = $payPal;
        $this->orderRepository = $orderRepository;
		$this->stampRepository = $stampRepository;
    }

    public function getCheckout()
    {
		$customer_id 		= auth()->user()->id;
		//$address 	= AddressDetails::where('id',auth()->user()->address_id)->first();
        return view('site.pages.checkout');
    }

    public function placeOrder(Request $request)
    {
		$customer_id = auth()->user()->id;
		
        $order = $this->orderRepository->storeOrderDetails($request->all());
        // You can add more control here to handle if the order is not stored properly
        /*if ($order) {
			if($request->payment_method == "Cash on Delivery"){
				$order->status = 'processing';
				$order->payment_status = 0;
				$order->delivery_type = 'Cash on Delivery';
				$order->save();
				cart()->clear();
				$orderItems = OrderDetails::where('order_id',$order->id)->get();
				return redirect()->route('order.success', ['orderId' => $order->order_id]);
				//return view('site.pages.success', compact('order'));
			}
			else{
				//$this->payPal->processPayment($order);
				return $this->showPayment($order);
			}
        }*/
		$updateStamp=$this->stampRepository->updateStamp($order->id,$request->redeem_points);
		
		if($order):
			$order->status = 'processing';
			$order->payment_status = 0;
			$order->save();
			cart()->clear();
			$orderItems = OrderDetails::where('order_id',$order->id)->get();
			
			return redirect()->route('order.success', ['orderId' => $order->order_id]);
		else:
			return redirect()->back()->with('message','Order not placed');
		endif;
        
    }

    public function complete(Request $request)
    {
        $paymentId = $request->input('paymentId');
        $payerId = $request->input('PayerID');

        $status = $this->payPal->completePayment($paymentId, $payerId);

        $order = Order::where('order_number', $status['invoiceId'])->first();
        $order->status = 'processing';
        $order->payment_status = 1;
        $order->payment_method = 'PayPal -'.$status['salesId'];
        $order->save();

        //Cart::clear();
		cart()->clear();
        return view('site.pages.success', compact('order'));
    }
	
	public function showPayment($order)
    {   
		return view('site.pages.payment',compact('order'));
    }
	
	public function orderSucess($orderNumber)
    {   
		$profile=auth()->user();
		$order = $this->orderRepository->findOrderByNumber($orderNumber);
		return view('site.pages.success',compact('order','profile'));
    }
	
	public function checkStamp(Request $request)
	{
		$valid=$this->stampRepository->checkStamp($request->stamp);
		return $valid;
	}
	
	public function checkVoucherCode(Request $request)
	{
		$valid=$this->stampRepository->checkVoucherCode($request->vouchercode);
		return $valid;
	}
}
