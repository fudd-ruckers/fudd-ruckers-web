<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use App\Contracts\OrderContract;
use App\Contracts\CustomerContract;
use App\Contracts\StampContract;
use App\Traits\UploadAble;

class AccountController extends Controller
{
	use UploadAble;
	
	protected $orderRepository;
	
	public function __construct(OrderContract $orderRepository, CustomerContract $customerRepository, StampContract $stampRepository)
	{
		$this->orderRepository = $orderRepository;
		$this->customerRepository = $customerRepository;
		$this->stampRepository = $stampRepository;
	}
    public function getOrders()
    {
        $orders = auth()->user()->orders;
        return view('site.pages.account.orders', compact('orders'));
    }
	
	public function getProfile()
	{
		$profile=auth()->user();
		$stamps = $this->stampRepository->getStampHistory();
		$stamplist = $max_date =[];
	    foreach($stamps as $stamp){
		   $max_date[] = strtotime($stamp->date);
		   $stamp_date = date('F d,Y', strtotime($stamp->date));
		   $stamplist[$stamp_date][]=$stamp;
	    }
		if(!empty($max_date))
			$profile->maxdate =  date('F d,Y',max($max_date));
		else
			$profile->maxdate = 'No Updates Found';
		return view('site.pages.account.edit_profile', compact('profile','stamplist'));
	}
	
	public function updateProfile(Request $request)
	{
		$validatedData = $request->validate([
      'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,'.auth()->user()->id],
    ]);
	
	$address = $this->customerRepository->updateUser($request);
		
		return \Redirect::route('account.profile');
	}
	
	public function viewOrder($orderNo)
	{
		$order = $this->orderRepository->findOrderByNumber($orderNo);
		return view('site.pages.account.showorder', compact('order'));
	}
	
	public function addAddress()
	{
		return view('site.pages.account.addaddress');
	}
	
	public function storeAddress(Request $request)
	{
		$address = $this->customerRepository->addCustomerAddress($request);
		return \Redirect::route('account.profile');
	}
	
	public function editProfile()
	{
		$profile=auth()->user();
		return view('site.pages.account.edit', compact('profile'));
	}
	
	public function changePassword()
	{
		$profile=auth()->user();
		return view('site.pages.account.password', compact('profile'));
	}
	
	public function updatePassword(Request $request)
	{
		if(Hash::check($request->old_password, auth()->user()->password)):
			$password = $this->customerRepository->updatePassword($request);
			return back()->with('success','Password changed');
		else:
			return back()->with('error','Old Password does not match');
		endif;
	}
	
	public function deleteAddress($id)
	{
		$delete = $this->customerRepository->deleteAddress($id);
		return \Redirect::route('account.profile');
	}
	
	public function editAddress($id)
	{
		$address = $this->customerRepository->getAddress($id);
		return view('site.pages.account.editaddress', compact('address'));
	}
	
	public function updateAddress(Request $request)
	{
		
		$address = $this->customerRepository->updateAddress($request);
		return \Redirect::route('account.profile');
	}
	public function defaultAddress()
	{
		$addressId = request('id');
		$address = $this->customerRepository->defaultAddress($addressId);
		return response()->json([
            'message' => 'Default Address Added Successfully',
			'status' => true,
			'request' => $addressId,
        ], 200);
	}
}
