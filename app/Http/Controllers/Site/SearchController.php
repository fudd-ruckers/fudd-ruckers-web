<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Menu;

class SearchController extends Controller
{
    public function index(Request $request)
	{
		$search_item=$request->search;
		$products=Menu::with('images')->with('attributes')
		->where('name',$request->search)
		->orWhere('name', 'like', '%' . $search_item . '%')
		->orWhere('tags', 'like', '%' . $search_item . '%')
		->orWhere('details_text', 'like', '%' . $search_item . '%')
		->paginate(12)
		->appends(request()->query());
		return view('site.pages.shop',compact('products','search_item'));
	}
}
