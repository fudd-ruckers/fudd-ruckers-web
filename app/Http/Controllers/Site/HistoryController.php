<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Contracts\OrderContract;

class HistoryController extends Controller
{
	protected $orderRepository;
	
	public function __construct(OrderContract $orderRepository)
	{
		$this->orderRepository = $orderRepository;
	}
	
    public function index(Request $request)
    {
	   $orders = auth()->user()->order_history;
	   $orderlist =[];
	   foreach($orders as $order){
		   $order_date = date('F d,Y', strtotime($order->created_at));
		   $orderlist[$order_date][$order->id][]=$order;
	   }
	   
       return view('site.pages.order_history', compact('orderlist'));
    }
}
