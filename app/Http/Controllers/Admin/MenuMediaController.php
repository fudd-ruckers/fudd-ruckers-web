<?php

namespace App\Http\Controllers\Admin;

use App\Traits\UploadAble;
use App\Models\MenuMedia;
use Illuminate\Http\Request;
use App\Contracts\MenuContract;
use App\Http\Controllers\Controller;

class MenuMediaController extends Controller
{
    use UploadAble;

    protected $productRepository;

    public function __construct(MenuContract $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function upload(Request $request)
    {
        $product = $this->productRepository->findProductById($request->product_id);
        if ($request->has('image')) {
            $image = $this->uploadOne($request->image, 'menus');
            $productImage = new MenuMedia([
				'media_type'=>1,
                'media_file'      =>  $image,
            ]);
			
            $product->images()->save($productImage);
        }

        return response()->json(['status' => 'Success']);
    }
	
	public function mediaUpload(Request $request)
	{
		$product = $this->productRepository->findProductById($request->productid);
		
		if($request->has('arlink') && $request->arlink!=NULL):
			$link = new MenuMedia([
				'media_type'=>3,
                'media_file'      =>  $request->arlink,
            ]);
			$product->images()->save($link);
		endif;
		
		if($request->has('video')):
			$video = $this->uploadOne($request->video, 'menus');
            $productImage = new MenuMedia([
				'media_type'=>2,
                'media_file'      =>  $video,
            ]);
			
            $product->images()->save($productImage);
		endif;
		return redirect()->back();
	}

    public function delete($id)
    {
        $image = MenuMedia::findOrFail($id);

        if ($image->full != '') {
            $this->deleteOne($image->full);
        }
        $image->delete();

        return redirect()->back();
    }
}
