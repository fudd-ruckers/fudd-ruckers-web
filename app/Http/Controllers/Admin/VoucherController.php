<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Contracts\VoucherContract;

class VoucherController extends BaseController
{
	/**
     * @var UserContract
     */
    protected $customerRepository;
    /**
     * CategoryController constructor.
     * @param UserContract $voucherRepository
     */
    public function __construct(VoucherContract $voucherRepository)
    {
        $this->voucherRepository = $voucherRepository;
    }
	
	/**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $vouchers = $this->voucherRepository->listVouchers();
        $this->setPageTitle('Vouchers', 'List of all Vouchers');
        return view('admin.voucher.index',compact('vouchers'));
    }
	
	public function create()
    {
        $this->setPageTitle('Voucher', 'Create Voucher');
        return view('admin.voucher.create');
    }
	
	/**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'code'      =>  'required|max:191|unique:vouchers',
			'description'      =>  'required',
			'type'      =>  'required',
			'value'      =>  'required',
        ]);

        $params = $request->except('_token');

        $voucher = $this->voucherRepository->createVoucher($params);

        if (!$voucher) {
            return $this->responseRedirectBack('Error occurred while creating voucher.', 'error', true, true);
        }
        return $this->responseRedirect('admin.voucher.index', 'Voucher added successfully' ,'success',false, false);
    }
	
	/**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $targetVoucher = $this->voucherRepository->findVoucherById($id);
        $this->setPageTitle('Voucher', 'Edit Voucher : '.$targetVoucher->name);
        return view('admin.voucher.edit', compact('targetVoucher'));
    }

	/**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'code'      =>  ['required', 'string', 'max:191', 'unique:vouchers,code,'.$request->id],
			'description'      =>  'required',
			'type'      =>  'required',
			'value'      =>  'required',
        ]);

        $params = $request->except('_token');

        $voucher = $this->voucherRepository->updateVoucher($params);

        if (!$voucher) {
            return $this->responseRedirectBack('Error occurred while updating voucher.', 'error', true, true);
        }
        return $this->responseRedirectBack('Voucher updated successfully' ,'success',false, false);
    }
	
	/**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $voucher = $this->voucherRepository->deleteVoucher($id);

        if (!$voucher) {
            return $this->responseRedirectBack('Error occurred while deleting voucher.', 'error', true, true);
        }
        return $this->responseRedirect('admin.voucher.index', 'Voucher deleted successfully' ,'success',false, false);
    }
}
