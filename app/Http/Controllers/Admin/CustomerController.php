<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\CustomerContract;
use App\Http\Controllers\BaseController;

class CustomerController extends BaseController
{
    /**
     * @var UserContract
     */
    protected $customerRepository;

    /**
     * CategoryController constructor.
     * @param UserContract $customerRepository
     */
    public function __construct(CustomerContract $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $customers = $this->customerRepository->listUsers();
        $this->setPageTitle('Customers', 'List of all customers');
        return view('admin.customers.index', compact('customers'));
    }
	
	/**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $user = $this->customerRepository->findUserById($id);
        $this->setPageTitle('Customers', 'Edit Customer : '.$user->name);
        return view('admin.customers.edit', compact('user'));
    }
	
	/**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request)
    {
		$this->validate($request, [
            'name'      =>  'required|max:191',
			'email' => ['required', 'string', 'email', 'max:255', 'unique:customers,email,'.$request->id],
			'phone'      =>  'required|numeric',
			'address'      =>  'required|max:191',
			'weight'      =>  'required|max:191',
			'age'      =>  'required|max:191',
			'sex'      =>  'required|max:191',
        ]);
		
		$params = $request->except('_token');
		$user = $this->customerRepository->updateUser($params);
		if (!$user) {
            return $this->responseRedirectBack('Error occurred while updating user.', 'error', true, true);
        }
        return $this->responseRedirectBack('User updated successfully' ,'success',false, false);
	}
	
	/**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	 public function destroy($id)
	 {
		$delete = $this->customerRepository->deleteUser($id);
		if(!$delete):
			return $this->responseRedirectBack('Error occurred while deleting user.', 'error', true, true);
		endif;
		return $this->responseRedirectBack('User deleted successfully' ,'success',false, false);
	 }
	 
	 /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	 public function activate_user(Request $request)
	 {
		 $user = $this->customerRepository->activate_user($request->customerid);
		 if($user):
		 echo "success";
		 else:
		 echo "error";
		 endif;
	 }
}
