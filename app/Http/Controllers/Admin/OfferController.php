<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Contracts\OfferContract;
use App\Traits\UploadAble;

class OfferController extends BaseController
{
	 /**
     * @var OfferContract
     */
    protected $offerRepository;
	use UploadAble;
    /**
     * OfferController constructor.
     * @param OfferContract $offerRepository
     */
	 
    public function __construct(OfferContract $offerRepository)
    {
        $this->offerRepository = $offerRepository;
    }
	
	/**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $offers = $this->offerRepository->listOffers();
        $this->setPageTitle('Offers', 'List of all Offers');
        return view('admin.offers.index',compact('offers'));
    }
	
	public function create()
    {
        $this->setPageTitle('Offer', 'Create Offer');
        return view('admin.offers.create');
    }
	
	
	public function storeMedia(Request $request)
	{
		$file = $request->file('file');;
		$response = $this->uploadTemp( $file,'tmp/offers');
		
		return $response;
	}
	
	/**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'      =>  'required|max:191',
			'image'      =>  'required',
        ]);
		
		$this->moveOne($request->image);
        $params = $request->except('_token');
		

        $offer = $this->offerRepository->createOffer($params);

        if (!$offer) {
            return $this->responseRedirectBack('Error occurred while creating offer.', 'error', true, true);
        }
        return $this->responseRedirect('admin.offer.index', 'Offer added successfully' ,'success',false, false);
    }
	
	/**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $targetOffer = $this->offerRepository->findOfferById($id);
		$targetOffer->size = $this->imageSize($targetOffer->image);
        $this->setPageTitle('Offer', 'Edit Offer : '.$targetOffer->title);
        return view('admin.offers.edit', compact('targetOffer'));
    }

	/**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'title'      =>  ['required', 'string', 'max:191'],
			'image'      =>  'required',
        ]);

        $params = $request->except('_token');

        $offer = $this->offerRepository->updateOffer($params);

        if (!$offer) {
            return $this->responseRedirectBack('Error occurred while updating offer.', 'error', true, true);
        }
        return $this->responseRedirectBack('Offer updated successfully' ,'success',false, false);
    }
	
	/**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $offer = $this->offerRepository->deleteOffer($id);

        if (!$offer) {
            return $this->responseRedirectBack('Error occurred while deleting offer.', 'error', true, true);
        }
        return $this->responseRedirect('admin.offer.index', 'Offer deleted successfully' ,'success',false, false);
    }
}
