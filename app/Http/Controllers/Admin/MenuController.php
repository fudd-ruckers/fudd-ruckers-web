<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Contracts\CategoryContract;
use App\Contracts\MenuContract;
use App\Http\Controllers\BaseController;
use App\Http\Requests\StoreMenuFormRequest;

class MenuController extends BaseController
{
    protected $categoryRepository;

    protected $menuRepository;

    public function __construct(
        CategoryContract $categoryRepository,
        MenuContract $menuRepository
    )
    {
        $this->categoryRepository = $categoryRepository;
        $this->menuRepository = $menuRepository;
    }

    public function index()
    {
        $products = $this->menuRepository->listProducts();
        $this->setPageTitle('Menu', 'Menu List');
        return view('admin.menus.index', compact('products'));
    }

    public function create()
    {
        $categories = $this->categoryRepository->listCategories('name', 'asc');
		$menus = $this->menuRepository->getSubProducts();
        $this->setPageTitle('Menu', 'Create Menu');
        return view('admin.menus.create', compact('categories','menus'));
    }

    public function store(StoreMenuFormRequest $request)
    {
        $params = $request->except('_token');
        $product = $this->menuRepository->createProduct($params);

        if (!$product) {
            return $this->responseRedirectBack('Error occurred while creating product.', 'error', true, true);
        }
        return $this->responseRedirect('admin.menu.index', 'Product added successfully' ,'success',false, false);
    }

    public function edit($id)
    {
        $product = $this->menuRepository->findProductById($id);
        $categories = $this->categoryRepository->listCategories('name', 'asc');
		$menus = $this->menuRepository->listProducts();
        $this->setPageTitle('Menu', 'Edit Menu');
        return view('admin.menus.edit', compact('categories', 'product','menus'));
    }

    public function update(StoreMenuFormRequest $request)
    {
        $params = $request->except('_token');

        $product = $this->menuRepository->updateProduct($params);

        if (!$product) {
            return $this->responseRedirectBack('Error occurred while updating product.', 'error', true, true);
        }
        return $this->responseRedirect('admin.menu.index', 'Product updated successfully' ,'success',false, false);
    }
}
