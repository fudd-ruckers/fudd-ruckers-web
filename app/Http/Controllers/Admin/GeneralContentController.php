<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Repositories\GeneralContentRepository;

class GeneralContentController extends BaseController
{
	protected $generalRepository;
	public function __construct(GeneralContentRepository $generalRepository)
    {
        $this->generalRepository = $generalRepository;
    }
    public function index()
	{
		$this->setPageTitle('General', 'General Content');
		$general=$this->generalRepository->getContent();
        return view('admin.general.index',compact('general'));
	}
	
	public function update(Request $request)
	{
		if ($request->has('terms_and_conditions_text'))
		{
			$terms=$this->generalRepository->updateTerms($request->terms_and_conditions_text);
		}
		elseif($request->has('privacy_policy_text'))
		{
			$terms=$this->generalRepository->updatePrivacy($request->privacy_policy_text);
		}
		elseif($request->has('about_us_text'))
		{
			$terms=$this->generalRepository->updateAboutus($request->about_us_text);
		}
		return $this->responseRedirectBack('Settings updated successfully.', 'success');
	}
}
