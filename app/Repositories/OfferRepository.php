<?php

namespace App\Repositories;

use App\Models\Offer;
use App\Contracts\OfferContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;

/**
 * Class OfferRepository
 *
 * @package \App\Repositories
 */
class OfferRepository extends BaseRepository implements OfferContract
{
    /**
     * OfferRepository constructor.
     * @param Offer $model
     */
    public function __construct(Offer $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }
	
	public function listOffers(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }
	
	/**
     * @param int $id
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findOfferById(int $id)
    {
        try {
            return $this->findOneOrFail($id);

        } catch (ModelNotFoundException $e) {

            throw new ModelNotFoundException($e);
        }

    }
	
	/**
     * @param array $params
     * @return Category|mixed
     */
    public function createOffer(array $params)
    {
        try {
            $collection = collect($params);
			$status = $collection->has('status') ? 1 : 0;
            $offer = new Offer();
			$offer->title=$params['title'];
			$offer->details=$params['details'];
			$offer->image=$params['image'];
			
			$offer->status=$status;
            $offer->save();
            return $offer;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }
	
	/**
     * @param array $params
     * @return mixed
     */
    public function updateOffer(array $params)
    {
        $offer = $this->findOfferById($params['id']);

        $collection = collect($params)->except('_token');

		$status = $collection->has('status') ? 1 : 0;
		$offer->title=$params['title'];
		$offer->details=$params['details'];
		$offer->image=$params['image'];
		
		$offer->status=$status;
        $offer->save();

        return $offer;
    }
	
	/**
     * @param $id
     * @return bool|mixed
     */
    public function deleteOffer($id)
    {
        $offer = $this->findOfferById($id);

        $offer->delete();

        return $offer;
    }
}
