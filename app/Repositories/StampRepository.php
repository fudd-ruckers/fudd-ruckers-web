<?php

namespace App\Repositories;

use App\Models\StampHistory;
use App\Models\Customer;
use App\Models\Voucher;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;
use App\Contracts\StampContract;

/**
 * Class StampRepository
 *
 * @package \App\Repositories
 */
class StampRepository extends BaseRepository implements StampContract
{
    /**
     * StampRepository constructor.
     * @param StampHistory $model
     */
    public function __construct(StampHistory $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }
	
	public function updateStamp($orderid,$redeemPoint)
	{
		$stamp=1;
		$items = cart()->items();
		$totalQunatity = 0;
		foreach ($items as $item)
		{
			$totalQunatity += $item['quantity'];
		}
		$totalStamp=$stamp*$totalQunatity;
		
		$this->updateAddedStamp($totalStamp,$orderid);
		if($redeemPoint!=null):
			$this->updateRedeemStamp($redeemPoint,$orderid);
		endif;
		$this->updateCustomerStamp($totalStamp,$redeemPoint);
	}
	
	public function updateCustomerStamp($totalStamp,$redeemPoint)
	{
		$customer_id=auth()->user()->id;
		$customer=Customer::where('id',$customer_id)->first();
		$current_stamp=$customer->no_of_stamps;
		$totalStamp=$totalStamp+$current_stamp-$redeemPoint;
		return Customer::where('id',$customer_id)->update(['no_of_stamps' => $totalStamp]);
	}
	
	public function updateAddedStamp($totalStamp,$orderid)
	{
		$customer_id=auth()->user()->id;
		$customer=Customer::where('id',$customer_id)->first();
		$current_stamp=$customer->no_of_stamps;
		
		return StampHistory::create(['order_id' => $orderid,'customer_id' => $customer_id,'current_stamps' =>$current_stamp,'stamps' => $totalStamp,'type'=>1]);
	}
	
	public function updateRedeemStamp($redeemPoint,$orderid)
	{
		$customer_id=auth()->user()->id;
		$customer=Customer::where('id',$customer_id)->first();
		$current_stamp=$customer->no_of_stamps;
		
		return StampHistory::create(['order_id' => $orderid,'customer_id' => $customer_id,'current_stamps' =>$current_stamp,'stamps' => $redeemPoint,'type'=>2]);
	}
	
	public function checkStamp($stamp)
	{
		$customer_id=auth()->user()->id;
		$customer=Customer::where('id',$customer_id)->first();
		$current_stamp=$customer->no_of_stamps;
		if($current_stamp>$stamp):
		return 1;
		else:
		return 0;
		endif;
	}
	
	public function checkVoucherCode($code)
	{
		$voucher=Voucher::where('code',$code)->first();
		if(empty($voucher)):
			return 0;
		else:
			return 1;
		endif;
	}
 
	public function getStampHistory()
	{
		$customer_id=auth()->user()->id;
		$stamps=StampHistory::where('customer_id',$customer_id)->orderBy('date','desc')->get();
        return $stamps;
	}
}
