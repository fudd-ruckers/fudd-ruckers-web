<?php

namespace App\Repositories;

use App\Models\CartItem;
use App\Models\Menu;
use App\Contracts\CartSubItemContract;
use App\Models\CartSubItem;

class CartSubItemRepository extends BaseRepository implements CartSubItemContract
{
    public function __construct(CartSubItem $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }
	
	public function getCartItemId($menuId)
	{
		return CartItem::where('model_id',$menuId)
			->where('cart_id',cart()->id())
			->first();
	}
	
	public function clearSubItem($menuId)
	{
		$cartItem=$this->getCartItemId($menuId);
		return CartSubItem::where('cart_id',cart()->id())
		->where('cart_item_id',$cartItem->id)
		->delete(); 
	}
	
	public function createCartAddons($request,$menuId)
    {
		$cartItem=$this->getCartItemId($menuId);
		
		foreach($request as $addon):
		
			$subItem=new CartSubItem();
			$subItem->cart_id=cart()->id();
			$subItem->cart_item_id=$cartItem->id;
			$subItem->menu_id=$addon;
			$subItem->type=1;
			$subItem->save();
		endforeach;
		
		return $subItem;
	}
	
	public function createCartDrinks($request,$menuId)
    {
		$cartItem=$this->getCartItemId($menuId);
		
		foreach($request as $drink):
			
			$subItem=new CartSubItem();
			$subItem->cart_id=cart()->id();
			$subItem->cart_item_id=$cartItem->id;
			$subItem->menu_id=$drink;
			$subItem->type=2;
			$subItem->save();
		endforeach;
		
		return $subItem;
	}
	
	public function getAddonPrice($addons)
	{
		$total=0;
		foreach($addons as $addon):
			$price=$this->getPrice($addon);
			$total=$total+$price;
		endforeach;
		return $total;
	}
	
	public function getDrinkPrice($drinks)
	{
		$total=0;
		foreach($drinks as $drink):
			$price=$this->getPrice($drink);
			$total=$total+$price;
		endforeach;
		return $total;
	}
	
	public function getPrice($menuid)
	{
		$menu=Menu::where('id',$menuid)->first();
		return $menu->price;
	}
	
	public function updateNetTotal($totalPrice)
	{
		$netTotal=\DB::table('carts')
					->where('id', cart()->id())
					->limit(1)
					->update(array('net_total' => $totalPrice));
		return $netTotal;
	}
	
	public function updateTotal($totalPrice)
	{
		$tax=cart()->tax();
		$netTotal=\DB::table('carts')
					->where('id', cart()->id())
					->limit(1)
					->update(array('total' => $totalPrice+$tax));
		return $netTotal;
	}
	
	public function updatePayable($totalPrice)
	{
		$tax=cart()->tax();
		$netTotal=\DB::table('carts')
					->where('id', cart()->id())
					->limit(1)
					->update(array('payable' => $totalPrice+$tax));
		return $netTotal;
	}
}
