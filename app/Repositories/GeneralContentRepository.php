<?php

namespace App\Repositories;

use App\Models\General;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;

/**
 * Class GeneralContentRepository
 *
 * @package \App\Repositories
 */
class GeneralContentRepository extends BaseRepository
{

    /**
     * GeneralContentRepository constructor.
     * @param General $model
     */
    public function __construct(General $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

	/**
     * @param array $params
     * @return mixed
     */
	public function updateTerms($params)
    {
		$terms = General::get();
		if(count($terms)<=0):
		$general=new General();
		$general->terms_conditions=$params;
		$general->save(); 
		else:
		General::limit(1)->update(array('terms_conditions' => $params));
		endif;
	}
	
	/**
     * @param array $params
     * @return mixed
     */
	public function updatePrivacy($params)
    {
		$terms = General::get();
		if(count($terms)<=0):
		$general=new General();
		$general->privacy_policy=$params;
		$general->save(); 
		else:
		General::limit(1)->update(array('privacy_policy' => $params));
		endif;
	}
	
	/**
     * @param array $params
     * @return mixed
     */
	public function updateAboutus($params)
    {
		$terms = General::get();
		if(count($terms)<=0):
		$general=new General();
		$general->aboutus=$params;
		$general->save(); 
		else:
		General::limit(1)->update(array('aboutus' => $params));
		endif;
	}
	/**
     * @param array $params
     * @return mixed
     */
	 public function getContent()
	 {
		$general=General::get()->first();
		return $general;
	 }
}
