<?php

namespace App\Repositories;

use App\Models\Customer;
use App\Models\AddressDetails;
use App\Contracts\CustomerContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Hash;
use Doctrine\Instantiator\Exception\InvalidArgumentException;
use App\Traits\UploadAble;

/**
 * Class CategoryRepository
 *
 * @package \App\Repositories
 */
class CustomerRepository extends BaseRepository implements CustomerContract
{
	use UploadAble;
    /**
     * CategoryRepository constructor.
     * @param Customer $model
     */
    public function __construct(Customer $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function listUsers(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
        //return $this->all($columns, $order, $sort);
		return Customer::orderBy("customers.id","desc")
		->get();
    }

    /**
     * @param int $id
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findUserById(int $id)
    {
        return Customer::join('address_details', 'customers.address_id', '=', 'address_details.id')
		->where("customers.id",$id)
		->first();
    }

    /**
     * @param array $params
     * @return User|mixed
     */
    public function createUser(array $params)
    {
        try {
            $collection = collect($params);

            $logo = null;

            if ($collection->has('logo') && ($params['logo'] instanceof  UploadedFile)) {
                $logo = $this->uploadOne($params['logo'], 'brands');
            }

            $merge = $collection->merge(compact('logo'));

            $brand = new User($merge->all());

            $brand->save();

            return $brand;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function updateUser($params)
    {
		$collection = collect($params);
		$offers = $collection->has('offers_discounts') ? 1 : 0;
		$newsletter = $collection->has('newsletter') ? 1 : 0;
		if($collection->has('image')):
			$image = $this->uploadOne($params->image, 'customers');
		endif;
		
        return Customer::where('id',auth()->user()->id)->update(array("first_name"=>$params['first_name'],"last_name"=>$params['last_name'],"email"=>$params['email'],"mobile_number"=>$params['mobile_number'],"sex"=>$params['sex'],"offers"=>$offers,"newsletter"=>$newsletter,"image"=>$image));
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    public function deleteUser($id)
    {
        $user = $this->findUserById($id);

        if ($user->logo != null) {
            $this->deleteOne($user->logo);
        }

        $user->delete();

        return $user;
    }
	
	/**
     * @param $id
     * @return bool|mixed
     */
	 public function activate_user($id)
	 {
		//$user=User::withTrashed()->where('id',$id)
		//->get();
		$user=Customer::withTrashed()->find($id);
		$user->deleted_at=NULL;
		$user->save();
		return $user;
	 }
	 
	 public function addCustomerAddress($request)
	 {
		//dd(auth()->user()->id);
		$address=new AddressDetails();
		$address->customer_id=auth()->user()->id;
		$address->address_alias=$request->address_alias;
		$address->flat_number=$request->flat_number;
		$address->floor_number=$request->floor_number;
		$address->area=$request->area;
		$address->block_number=$request->block_number;
		$address->road_number=$request->road_number;
		$address->additional_information=$request->additional_information;
		
		//set first address as default address
		$address->is_default = $this->checkAddressCount($address->customer_id);
		
		$address->save();
		
		return $address;
	 }
	 
	 public function updatePassword($request)
	 {
		$password=Customer::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);
		return $password;
	 }
	 
	 public function deleteAddress($id)
	 {
		$delete=AddressDetails::find($id)->delete();
		return $delete;
	 }
	 
	  public function getAddress($id)
	 {
		$address=AddressDetails::find($id);
		return $address;
	 }
	 
	 public function updateAddress($request)
	 {
		$address=AddressDetails::find($request->id)->update(['address_alias'=> $request->address_alias,'flat_number'=> $request->flat_number,'floor_number'=> $request->floor_number,'area'=> $request->area,'block_number'=> $request->block_number,'road_number'=> $request->road_number,'additional_information'=> $request->additional_information]);
		
		return $address;
	 }
	 
	 public function checkAddressCount($customer)
	{
		$address=AddressDetails::where('customer_id',$customer)->first();
		if(empty($address)):
			return 1;
		else:
			return 0;
		endif;
	}
	
	public function defaultAddress($addressId)
	 {
		$reset_address=AddressDetails::where('customer_id',auth()->user()->id)->update(['is_default'=> 0]);
		$address=AddressDetails::where('id',$addressId)->update(['is_default'=> 1]);
		
		return $address;
	 }
}
