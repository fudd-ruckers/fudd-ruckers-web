<?php

namespace App\Repositories;

//use Cart;
use App\Models\Order;
use App\Models\Menu;
use App\Models\Customer;
use App\Models\OrderDetails;
use App\Models\CartSubItem;
use App\Models\CartItemAttribute;
use App\Models\OrderSubItem;
use App\Models\OrderItemAttribute;
use App\Models\Stamp;
use App\Contracts\OrderContract;

class OrderRepository extends BaseRepository implements OrderContract
{
    public function __construct(Order $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    public function storeOrderDetails($params)
    {
		$items = cart()->items();
		$totalQunatity = 0;
		$stamp=1;
		foreach ($items as $item)
		{
			$totalQunatity += $item['quantity'];
		}
		
		$customer_id=auth()->user()->id;
        $order = Order::create([
            'order_id'      =>  $params['order_no'],
            'customer_id'   =>  $customer_id,
			'value'       =>  cart()->payable(),
            'status'       =>  'pending',
            'delivery_type'        =>  $params['delivery_type'],
			'payment_status'    =>0,
            'address_id'    =>  $params['address_id'],
            'store_id'    =>  1,
			'order_date'    =>  date('YY-mm-dd'),
			'voucher_code'    =>  $params['voucher_code'],
			'points'    =>  $totalQunatity*$stamp,
			'table_no'    =>  $params['table_no'],
			'special_instruction'    =>  $params['special_instruction'],
        ]);
        if ($order) {
			
            $items = cart()->items();
            foreach ($items as $item)
            {
				$itemStamp=$item['quantity']*$stamp;
                // A better way will be to bring the product id with the cart items
                // you can explore the package documentation to send product id with the cart
                $product = Menu::where('id', $item['modelId'])->first();
                $orderItem = new OrderDetails([
					'order_id'    =>  $order->id,
                    'menu_id'    =>  $product->id,
                    'stamp_applied'      =>  $itemStamp,
                    'quantity'         =>  $item['quantity'],
					'value'         =>  $item['price'],
					'category_id'         =>  $product->category_id,
                ]);
                $order->items()->save($orderItem);
				
				$updateItemStamp=$this->updateItemStamp($product->id,$item['quantity']);
				
				$subItems=$this->checkSubItems($item['id']);
				if(!empty($subItems)):
					foreach($subItems as $sub){
						$orderSubItem=new OrderSubItem([
						'order_id'	=>	$orderItem->order_id,
						'order_item_id'	=>	$orderItem->id,
						'menu_id'	=>	$sub->menu_id,
						'type'	=>	$sub->type,
						]);
						
						$orderSubItem->save();
					}
				endif;
				
				$itemAttributes=$this->checkItemAttributes($item['id']);
				if(!empty($itemAttributes)):
					foreach($itemAttributes as $attribute){
						$orderAttributes=new OrderItemAttribute([
						'order_id'	=>	$orderItem->order_id,
						'order_item_id'	=>	$orderItem->id,
						'attribute_id'	=>	$attribute->attribute_id,
						'attribute_value_id'	=>	$attribute->attribute_value_id,
						]);
						
						$orderAttributes->save();
					}
				endif;
            }
        }
		
        return $order;
    }

    public function listOrders(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }

    public function findOrderByNumber($orderNumber)
    {
        return Order::where('order_id', $orderNumber)->first();
    }
	
	public function checkSubItems($cartItemId)
	{
		$cartId=cart()->id();
		return CartSubItem::where('cart_id',$cartId)->where('cart_item_id',$cartItemId)->get();
	}
	
	public function checkItemAttributes($cartItemId)
	{
		$cartId=cart()->id();
		return CartItemAttribute::where('cart_id',$cartId)->where('cart_item_id',$cartItemId)->get();
	}
	
	public function updateItemStamp($productid,$quantity)
	{
		$stamp=1;
		$totalStamp=$quantity*$stamp;
		$stamp=Stamp::where('menu_id',$productid)->first();
		if(!empty($stamp)):
			$totalStamp=$stamp->stamp_collection+$totalStamp;
			return Stamp::where('menu_id',$productid)
					->update(['stamp_collection'=>$totalStamp]);
		else:
			return Stamp::create(['menu_id' => $productid,'stamp_collection' => $totalStamp]);
		endif;
	}
	
	public function itemsByOrder($orderID)
    {
        $items=Order::where('orders.id', $orderID)->select('order_details.*')
        ->join('order_details', 'order_details.order_id', '=', 'orders.id')
        ->get();
        return $items;
    }
}
