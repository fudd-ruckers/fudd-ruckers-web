<?php

namespace App\Repositories;

use App\Models\Menu;
use App\Models\MenuSubItem;
use App\Models\ProductCategory;
use App\Traits\UploadAble;
use Illuminate\Http\UploadedFile;
use App\Contracts\MenuContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;

/**
 * Class ProductRepository
 *
 * @package \App\Repositories
 */
class MenuRepository extends BaseRepository implements MenuContract
{
    use UploadAble;

    /**
     * ProductRepository constructor.
     * @param Product $model
     */
    public function __construct(Menu $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function listProducts(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
		$product=Menu::select('menus.id','menus.name','menus.tags','menus.details_text','menus.price','menus.status','categories.name as category_name')
		->join('categories', 'categories.id', '=', 'menus.category_id')
		->get();
        return $product;
    }
	
	/**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function getSubProducts(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
		$product=Menu::select('menus.id','menus.name','menus.tags','menus.details_text','menus.price','menus.status')->where('status',1)
		->get();
        return $product;
    }

    /**
     * @param int $id
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findProductById(int $id)
    {
        try {
            return $this->findOneOrFail($id);

        } catch (ModelNotFoundException $e) {

            throw new ModelNotFoundException($e);
        }

    }

    /**
     * @param array $params
     * @return Product|mixed
     */
    public function createProduct(array $params)
    {
        try {
            $collection = collect($params);
			$status = $collection->has('status') ? 1 : 0;
            $product = new Menu();
			$product->name=$params['name'];
			$product->category_id=$params['category_id'];
			$product->tags=$params['tags'];
			$product->short_text=$params['short_text'];
			$product->details_text=$params['details_text'];
			$product->price=$params['price'];
			$product->status=$status;
            $product->save();
			
			if($collection->has('addons')):
				foreach($params['addons'] as $key=>$addon):
					$addOnArray[$key]['menu_id'] = $product->id;
					$addOnArray[$key]['item_id'] = $addon;
					$addOnArray[$key]['type'] = 1;
				endforeach;
				$product->addons()->sync($addOnArray);
			endif;
			
			if($collection->has('drinks')):
				foreach($params['drinks'] as $drinks):
					$product->drinks()->sync([0 => ["menu_id"=>$product->id,"item_id" => $drinks,"type"=>2]]);
				endforeach;
			endif;
			
            return $product;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function updateProduct(array $params)
    {
        $product = $this->findProductById($params['product_id']);
		$collection = collect($params);
		$status = $collection->has('status') ? 1 : 0;
        $product->update(array("name"=>$params['name'],"category_id"=>$params['category_id'],"tags"=>$params['tags'],"short_text"=>$params['short_text'],"details_text"=>$params['details_text'],"price"=>$params['price'],"status"=>$status));
		
		if($collection->has('addons')):
			foreach($params['addons'] as $key=>$addon):
				$addOnArray[$key]['menu_id'] = $params['product_id'];
				$addOnArray[$key]['item_id'] = $addon;
				$addOnArray[$key]['type'] = 1;
				$item=\DB::table('menu_sub_items')->where('menu_id', '=', $params['product_id'])->where('type',1)->delete();
			endforeach;
			$product->addons()->sync($addOnArray,false);	
		endif;
		
		if($collection->has('drinks')):
			foreach($params['drinks'] as $key=>$drink):
				$drinksArray[$key]['menu_id'] = $params['product_id'];
				$drinksArray[$key]['item_id'] = $drink;
				$drinksArray[$key]['type'] = 2;
				$item=\DB::table('menu_sub_items')->where('menu_id', '=', $params['product_id'])->where('type',2)->delete();
			endforeach;
			$product->drinks()->sync($drinksArray,false);	
		endif;
		
        return $product;
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    public function deleteProduct($id)
    {
        $product = $this->findProductById($id);

        $product->delete();

        return $product;
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function findProductBySlug($slug)
    {
        $product = Menu::with('images')->with('attributes')->where('slug', $slug)->first();
        return $product;
    }
	
	/**
     * @param 
     * @return mixed
     */
	 public function getProducts()
	 {
		$product = Menu::with('images')->with('attributes')->where('status',1)->get();
		return $product;
	 }
	 
	 /**
     * @param 
     * @return mixed
     */
	public function getFeaturedProducts($prductId='')
	{
		if($prductId > 0){
			$product = Menu::with('images')
					->where('id','<>',$prductId)
					->get();
		}else{
			$product = Menu::with('images')
					->with('attributes')
					->get();
		}
		return $product;
	}
	
	/**
     * @param 
     * @return mixed
     */
	public function getRelatedProducts($categoryIds,$productId)
	{
		/*$productCat   = ProductCategory::where('product_id','<>',$productId)
					 ->whereIn('category_id',$categoryIds)
					 ->get();
		$productIds	= $productCat->pluck('product_id')->toArray();*/
		
		$products = Menu::with('images')
				->where('id',$productId)
				->where('category_id',$categoryIds)
				->get();

		return $products;
	}
	
	/**
     * @param 
     * @return mixed
     */
	public function getAllProducts()
	{
		$products = Menu::with('images')->with('attributes')->where('status',1)->get();

		return $products;
	}
	
	/**
     * @param 
     * @return mixed
     */
	public function getShopProducts($sort='')
	{
		if($sort == 'name')
			$products = Menu::with('images')->orderBy('name', 'asc')->paginate(8);
		else if($sort == 'price_low_high')
			$products = Menu::with('images')->orderByRaw("CASE WHEN sale_price > 0 THEN sale_price ELSE price END ASC")->paginate(8);
		else if($sort == 'price_high_low')
			$products = Menu::with('images')->orderByRaw("CASE WHEN sale_price > 0 THEN sale_price ELSE price END DESC")->paginate(8);
		else if($sort == 'id')
			$products = Menu::with('images')->orderBy('id', 'desc')->paginate(8);
		else
			$products = Menu::with('images')->paginate(8);
		return $products;
	}
}
