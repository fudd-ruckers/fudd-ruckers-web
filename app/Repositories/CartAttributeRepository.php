<?php

namespace App\Repositories;

use App\Models\CartItem;
use App\Models\MenuAttribute;
use App\Contracts\CartAttributeContract;
use App\Models\CartItemAttribute;

class CartAttributeRepository extends BaseRepository implements CartAttributeContract
{
    public function __construct(CartItemAttribute $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }
	
	public function getCartItemId($menuId)
	{
		return CartItem::where('model_id',$menuId)
			->where('cart_id',cart()->id())
			->first();
	}
	
	public function clearAttribute($menuId)
	{
		$cartItem=$this->getCartItemId($menuId);
		return CartItemAttribute::where('cart_id',cart()->id())
		->where('cart_item_id',$cartItem->id)
		->delete(); 
	}
	
	public function createCartAttributes($request,$menuId)
    {
		$cartItem=$this->getCartItemId($menuId);
		foreach($request as $attribute):
			$attributes[] = [
				'cart_id' => cart()->id(),
				'cart_item_id' => $cartItem->id,
				'attribute_id' => $attribute[0],
				'attribute_value_id' => $attribute[1]
			];
		endforeach;
		
		return CartItemAttribute::insert($attributes);
	}
	
	public function getAttributePrice($request)
	{
		$total=0;
		foreach($request as $attribute):
			$price=$this->getPrice($attribute[1]);
			$total=$total+$price;
		endforeach;
		return $total;
	}
	
	public function getPrice($attributeId)
	{
		$attribute=MenuAttribute::where('id',$attributeId)->first();
		return $attribute->price;
	}
}
