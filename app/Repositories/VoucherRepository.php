<?php

namespace App\Repositories;

use App\Models\Voucher;
use App\Contracts\VoucherContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;

/**
 * Class VoucherRepository
 *
 * @package \App\Repositories
 */
class VoucherRepository extends BaseRepository implements VoucherContract
{
    /**
     * VoucherRepository constructor.
     * @param Voucher $model
     */
    public function __construct(Voucher $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }
	
	public function listVouchers(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }
	
	/**
     * @param int $id
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findVoucherById(int $id)
    {
        try {
            return $this->findOneOrFail($id);

        } catch (ModelNotFoundException $e) {

            throw new ModelNotFoundException($e);
        }

    }
	
	/**
     * @param array $params
     * @return Category|mixed
     */
    public function createVoucher(array $params)
    {
        try {
            $collection = collect($params);
			$status = $collection->has('status') ? 1 : 0;
            $voucher = new Voucher();
			$voucher->code=$params['code'];
			$voucher->description=$params['description'];
			$voucher->type=$params['type'];
			$voucher->value=$params['value'];
			$voucher->status=$status;
            $voucher->save();
            return $voucher;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }
	
	/**
     * @param array $params
     * @return mixed
     */
    public function updateVoucher(array $params)
    {
        $voucher = $this->findVoucherById($params['id']);

        $collection = collect($params)->except('_token');

		$status = $collection->has('status') ? 1 : 0;
		$voucher->code=$params['code'];
		$voucher->description=$params['description'];
		$voucher->type=$params['type'];
		$voucher->value=$params['value'];
		$voucher->status=$status;
        $voucher->save();

        return $voucher;
    }
	
	/**
     * @param $id
     * @return bool|mixed
     */
    public function deleteVoucher($id)
    {
        $voucher = $this->findVoucherById($id);

        $voucher->delete();

        return $voucher;
    }
}
