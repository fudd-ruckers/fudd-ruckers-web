<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerAddress extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'user_id', 'type', 'first_name', 'last_name', 'address', 'city', 'state', 'post_code', 'country', 'phone_number', 'email',
    ];
}
