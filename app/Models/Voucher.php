<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    /**
     * @var string
     */
    protected $table = 'vouchers';

    /**
     * @var array
     */
    protected $fillable = [
        'code', 'description', 'type', 'value', 'status',
    ];
}
