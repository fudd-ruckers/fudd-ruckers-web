<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';

    protected $fillable = [
        'order_id','customer_id','status','value','delivery_type','payment_status','address_id','store_id','voucher_code','points','table_no','special_instruction',
    ];

    public function user()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function items()
    {
        return $this->hasMany(OrderDetails::class);
    }
}
