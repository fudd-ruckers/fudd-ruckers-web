<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
     /**
     * @var string
     */
    protected $table = 'offers';

    /**
     * @var array
     */
    protected $fillable = [
        'title', 'detils', 'image', 'status',
    ];
}
