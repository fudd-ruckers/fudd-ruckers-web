<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuSubItem extends Model
{
    /**
     * @var string
     */
    protected $table = 'menu_sub_items';

    /**
     * @var array
     */
    protected $fillable = ['menu_id','item_id' ,'type'];
}
