<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetails extends Model
{
    protected $table = 'order_details';

    protected $fillable = [
        'order_id', 'menu_id', 'stamp_applied', 'quantity','value','category_id'
    ];

    public function product()
    {
        return $this->belongsTo(Menu::class, 'menu_id');
    }
}
