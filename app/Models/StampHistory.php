<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StampHistory extends Model
{
    /**
     * @var string
     */
    protected $table = 'stamp_history';

    /**
     * @var array
     */
    protected $fillable = [
        'order_id', 'customer_id', 'current_stamps', 'stamps', 'type', 'date',
    ];
}
