<?php
namespace App\Models;

use Config;
use Illuminate\Database\Eloquent\Model;

class AddressDetails extends Model
{
    /**
     * @var string
     */
    protected $table = 'address_details';
	/**
     * @var array
     */
    protected $fillable = ['address', 'address_alias','flat_number','floor_number','area','block_number','road_number','additional_information'];
}
