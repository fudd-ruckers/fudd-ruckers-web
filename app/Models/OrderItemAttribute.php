<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItemAttribute extends Model
{
    /**
     * @var string
     */
    protected $table = 'order_detail_attributes';
	/**
     * @var array
     */
    protected $fillable = ['order_id', 'order_item_id', 'attribute_id', 'attribute_value_id'];
}
