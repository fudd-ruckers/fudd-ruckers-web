<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    /**
     * @var string
     */
    protected $table = 'cart_items';

    /**
     * @var array
     */
    protected $fillable = ['cart_id','model_type' ,'model_id','name','price','image','quantity'];
}
