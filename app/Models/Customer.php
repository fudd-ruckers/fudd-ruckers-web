<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Authenticatable
{
	use HasApiTokens;
    use Notifiable;
	use SoftDeletes;
	/**
     * @var array
     */
    protected $fillable = ['first_name', 'last_name', 'email', 'mobile_number', 'password', 'facebook_id'];
	/**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
	
	public function orders()
    {
        return $this->hasMany(Order::class);
    }
	
	public function order_history()
    {
        return $this->hasMany(Order::class)
		->select('orders.*','order_details.id as details_id','order_details.menu_id','order_details.stamp_applied','order_details.quantity','order_details.value','order_details.category_id','menus.name as item_name','menus.price as item_price','menus.short_text')
		->join('order_details','orders.id', '=', 'order_details.order_id')
		->join('menus','order_details.menu_id', '=', 'menus.id')->orderby('orders.id','desc');
    }
	
	public function address()
	{
		return $this->hasMany(AddressDetails::class);
	}
	
	public function addNew($input)
    {
        $check = static::where('facebook_id',$input['facebook_id'])->first();


        if(is_null($check)){
            return static::create($input);
        }


        return $check;
    }
}
