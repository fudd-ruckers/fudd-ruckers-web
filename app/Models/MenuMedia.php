<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuMedia extends Model
{
    /**
     * @var string
     */
    protected $table = 'medias';

    /**
     * @var array
     */
    protected $fillable = ['media_type', 'media_file'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function menus()
    {
        return $this->belongsTo(Menu::class);
    }
}
