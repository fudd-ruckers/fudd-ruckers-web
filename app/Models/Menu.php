<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Freshbitsweb\LaravelCartManager\Traits\Cartable;

class Menu extends Model
{
	use Cartable;
	
    /**
     * @var string
     */
    protected $table = 'menus';

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'category_id', 'tags', 'short_text', 'details_text', 'price', 'status',
    ];
	
	/**
     * @param $value
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;
        $this->attributes['slug'] = Str::slug($value);
    }

    /**
     * @var array
     */
    protected $casts = [
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function addons()
    {
        return $this->belongsToMany(Menu::class, 'menu_sub_items', 'menu_id', 'item_id')->where('type',1);
    }
	
	 /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function drinks()
    {
        return $this->belongsToMany(Menu::class, 'menu_sub_items', 'menu_id', 'item_id')->where('type',2);
    }
	
	/**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany(MenuMedia::class)->where('media_type',1);
    }
	
	/**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attributes()
    {
        return $this->hasMany(MenuAttribute::class)->latest();
    }
}
