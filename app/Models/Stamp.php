<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stamp extends Model
{
    /**
     * @var string
     */
    protected $table = 'stamps';

    /**
     * @var array
     */
    protected $fillable = [
        'menu_id', 'stamp_collection', 'stamp_redemption',
    ];
}
