<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CartItemAttribute extends Model
{
    /**
     * @var string
     */
    protected $table = 'cart_item_attributes';
	/**
     * @var array
     */
    protected $fillable = ['cart_id', 'cart_item_id', 'attribute_id', 'attribute_value_id'];
}
