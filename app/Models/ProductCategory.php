<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    /**
     * @var string
     */
    protected $table = 'product_categories';

    /**
     * @var array
     */
    protected $fillable = [
        'category_id', 'product_id'
    ];
	
}
