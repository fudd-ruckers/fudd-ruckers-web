<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderSubItem extends Model
{
    /**
     * @var string
     */
    protected $table = 'order_detail_sub_items';
	/**
     * @var array
     */
    protected $fillable = ['order_id', 'order_item_id', 'menu_id', 'type'];
}
