<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuAttribute extends Model
{
    /**
     * @var string
     */
    protected $table = 'menu_attributes';

    /**
     * @var array
     */
    protected $fillable = ['attribute_id','value','quantity', 'price','menu_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Menu::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function attribute()
    {
        return $this->belongsTo(Attribute::class)->orderBy('id','asc');
    }
}
