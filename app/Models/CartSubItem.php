<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CartSubItem extends Model
{
    /**
     * @var string
     */
    protected $table = 'cart_sub_items';
	/**
     * @var array
     */
    protected $fillable = ['cart_id', 'cart_item_id', 'menu_id', 'type'];
}
