<?php

namespace App\Traits;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

/**
 * Trait UploadAble
 * @package App\Traits
 */
trait UploadAble
{
    /**
     * @param UploadedFile $file
     * @param null $folder
     * @param string $disk
     * @param null $filename
     * @return false|string
     */
    public function uploadOne(UploadedFile $file, $folder = null, $disk = 'public_uploads', $filename = null)
    {
        $name = !is_null($filename) ? $filename : Str::random(25);

        return $file->storeAs(
            $folder,
            $name . "." . $file->getClientOriginalExtension(),
            $disk
        );
    }

    /**
     * @param null $path
     * @param string $disk
     */
    public function deleteOne($path = null, $disk = 'public_uploads')
    {
        Storage::disk($disk)->delete($path);
    }
	
	 /**
     * @param null $frompath
     * @param null $topath
     */
    public function moveOne( $filename = null, $disk = 'public_uploads')
    {
		$frompath = 'tmp/offers/'.$filename;
		$topath = 'offer/'.$filename;
		
        Storage::disk($disk)->move($frompath, $topath);
    }
	
	 public function uploadTemp(UploadedFile $file, $folder = null, $disk = 'public_uploads', $filename = null)
    {
        $name = !is_null($filename) ? $filename : Str::random(25);

        $file->storeAs(
            $folder,
            $name . "." . $file->getClientOriginalExtension(),
            $disk
        );

		return response()->json([
			'name'          => $name,
			'ext'				=> $file->getClientOriginalExtension(),
			'original_name' => $file->getClientOriginalName(),
		]);
    }

	public function imageSize($filename, $disk = 'public_uploads')
	{
		$file_path = 'offer/'.$filename;
		return Storage::disk($disk)->size($file_path);
	}

}
