<?php

namespace App\Contracts;

/**
 * Interface MenuContract
 * @package App\Contracts
 */
interface VoucherContract
{
    /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function listVouchers(string $order = 'id', string $sort = 'desc', array $columns = ['*']);
	
	/**
     * @param int $id
     * @return mixed
     */
    public function findVoucherById(int $id);

}
