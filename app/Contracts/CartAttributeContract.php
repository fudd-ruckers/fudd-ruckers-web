<?php

namespace App\Contracts;

interface CartAttributeContract
{
	public function getCartItemId($menuId);
	public function clearAttribute($menuId);
}
