<?php

namespace App\Contracts;

interface CartSubItemContract
{
	public function getCartItemId($menuId);
    public function createCartAddons($params,$menuId);
	public function clearSubItem($menuId);
}
