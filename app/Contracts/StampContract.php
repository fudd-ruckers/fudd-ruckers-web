<?php

namespace App\Contracts;

/**
 * Interface MenuContract
 * @package App\Contracts
 */
interface StampContract
{
	/**
     * @param int $id
     * @return mixed
     */
    public function updateStamp($id,$point);
	
	public function updateCustomerStamp($totalStamp,$redeemPoint);
	
	public function updateAddedStamp($totalStamp,$orderid);
	
	public function updateRedeemStamp($redeemPoint,$orderid);
}
