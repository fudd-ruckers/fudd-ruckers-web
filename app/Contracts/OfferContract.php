<?php

namespace App\Contracts;

/**
 * Interface OfferContract
 * @package App\Contracts
 */
interface OfferContract
{
    /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function listOffers(string $order = 'id', string $sort = 'desc', array $columns = ['*']);
	
	/**
     * @param int $id
     * @return mixed
     */
    public function findOfferById(int $id);

}
