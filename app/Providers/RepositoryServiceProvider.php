<?php

namespace App\Providers;

use App\Contracts\CategoryContract;
use Illuminate\Support\ServiceProvider;
use App\Repositories\CategoryRepository;
use App\Contracts\AttributeContract;
use App\Repositories\AttributeRepository;
use App\Contracts\BrandContract;
use App\Contracts\MenuContract;
use App\Repositories\MenuRepository;
use App\Contracts\OrderContract;
use App\Repositories\OrderRepository;
use App\Contracts\CustomerContract;
use App\Repositories\CustomerRepository;
use App\Contracts\CartSubItemContract;
use App\Repositories\CartSubItemRepository;
use App\Contracts\CartAttributeContract;
use App\Repositories\CartAttributeRepository;
use App\Contracts\VoucherContract;
use App\Repositories\VoucherRepository;
use App\Contracts\OfferContract;
use App\Repositories\OfferRepository;
use App\Contracts\StampContract;
use App\Repositories\StampRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    protected $repositories = [
        CategoryContract::class         =>          CategoryRepository::class,
        AttributeContract::class        =>          AttributeRepository::class,
        MenuContract::class          =>          MenuRepository::class,
        OrderContract::class            =>          OrderRepository::class,
        CustomerContract::class             =>          CustomerRepository::class,
		CartSubItemContract::class             =>          CartSubItemRepository::class,
		CartAttributeContract::class             =>          CartAttributeRepository::class,
		VoucherContract::class             =>          VoucherRepository::class,
		OfferContract::class             =>          OfferRepository::class,
		StampContract::class             =>          StampRepository::class,
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        foreach ($this->repositories as $interface => $implementation)
        {
            $this->app->bind($interface, $implementation);
        }
    }
}
