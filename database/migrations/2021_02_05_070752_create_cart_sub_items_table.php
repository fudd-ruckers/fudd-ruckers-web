<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartSubItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_sub_items', function (Blueprint $table) {
            $table->increments('id');
			$table->unsignedBigInteger('cart_id');
			$table->unsignedBigInteger('cart_item_id');
			$table->unsignedBigInteger('menu_id');
			$table->boolean('type')->comment('1-addons , 2-drinks')->nullable(false);
			
			$table->foreign('cart_id')->references('id')->on('carts')->onDelete('cascade');
			$table->foreign('cart_item_id')->references('id')->on('cart_items')->onDelete('cascade');
			$table->foreign('menu_id')->references('id')->on('menus')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_sub_items');
    }
}
