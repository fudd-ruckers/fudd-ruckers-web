<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuSubItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_sub_items', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->unsignedBigInteger('menu_id')->index()->nullable();
			$table->unsignedBigInteger('item_id')->index()->nullable();
			$table->boolean('type')->comment('1-addons , 2-drinks')->nullable(false);
			$table->foreign('menu_id')->references('id')->on('menus')->onDelete('cascade');
			$table->foreign('item_id')->references('id')->on('menus')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_sub_items');
    }
}
