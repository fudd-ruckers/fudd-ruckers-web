<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartItemAttributes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_item_attributes', function (Blueprint $table) {
            $table->increments('id');
			$table->unsignedBigInteger('cart_id');
			$table->unsignedBigInteger('cart_item_id');
			$table->unsignedBigInteger('attribute_id');
			$table->unsignedBigInteger('attribute_value_id');
			
			$table->foreign('cart_id')->references('id')->on('carts')->onDelete('cascade');
			$table->foreign('cart_item_id')->references('id')->on('cart_items')->onDelete('cascade');
			$table->foreign('attribute_id')->references('id')->on('attributes')->onDelete('cascade');
			$table->foreign('attribute_value_id')->references('id')->on('menu_attributes')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_item_attributes');
    }
}
