<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->String('order_id')->nullable();
			$table->unsignedBigInteger('customer_id')->index();
			$table->enum('status', ['pending', 'processing', 'completed', 'decline'])->default('pending');
			$table->decimal('value', 8, 2)->nullable();
			$table->String('delivery_type')->nullable();
			$table->boolean('payment_status')->default(1);
			//$table->unsignedBigInteger('address_id')->index();
			$table->unsignedBigInteger('address_id')->nullable();
			$table->unsignedBigInteger('store_id')->nullable();
			
			$table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
			$table->foreign('address_id')->references('id')->on('address_details')->onDelete('cascade');
			//$table->foreign('store_id')->references('id')->on('store_lists')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
