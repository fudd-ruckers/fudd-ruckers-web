<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStampHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stamp_history', function (Blueprint $table) {
            $table->id();
			$table->unsignedBigInteger('order_id');
			$table->unsignedBigInteger('customer_id');
			$table->integer('current_stamps');
			$table->integer('stamps')->default(0);
			$table->enum('type', [1, 2])->comment('1-added , 2-redeemed')->nullable();
			$table->timestamp('date')->default(DB::raw('CURRENT_TIMESTAMP'));
			
			$table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
			$table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stamp_history');
    }
}
