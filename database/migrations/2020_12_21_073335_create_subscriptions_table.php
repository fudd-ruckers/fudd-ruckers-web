<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->String('subsription_id')->nullable();
			$table->unsignedBigInteger('customer_id')->index();
			$table->unsignedBigInteger('order_id')->index();
			$table->unsignedBigInteger('menu_id')->index();
			$table->unsignedBigInteger('address_id')->index();
			$table->dateTime('preferred_time');
			$table->String('menu_JPG');
			$table->date('start_date');
			$table->date('end_date');
			
			$table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
			$table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
			$table->foreign('menu_id')->references('id')->on('menus')->onDelete('cascade');
			$table->foreign('address_id')->references('id')->on('address_details')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions');
    }
}
