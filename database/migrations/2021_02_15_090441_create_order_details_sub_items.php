<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderDetailsSubItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_detail_sub_items', function (Blueprint $table) {
            $table->id();
			$table->unsignedBigInteger('order_id');
			$table->unsignedBigInteger('order_item_id');
			$table->unsignedBigInteger('menu_id');
			$table->enum('type', ['addons', 'drinks'])->comment('1-addons , 2-drinks')->nullable();
			
			$table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
			$table->foreign('order_item_id')->references('id')->on('order_details')->onDelete('cascade');
			$table->foreign('menu_id')->references('id')->on('menus')->onDelete('cascade');
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_detail_sub_items');
    }
}
