<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->String('first_name')->nullable();
			$table->String('last_name')->nullable();
			$table->String('email')->nullable();
			$table->unsignedBigInteger('mobile_number')->nullable();
			$table->string('password');
			$table->unsignedInteger('weight')->nullable();
			$table->unsignedInteger('age')->nullable();
			$table->String('sex')->nullable();

			//$table->foreign('stamp_id')->references('id')->on('stamp_details')->onDelete('cascade');
			
			$table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
