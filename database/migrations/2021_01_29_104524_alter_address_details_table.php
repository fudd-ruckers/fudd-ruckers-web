<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterAddressDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('address_details', function (Blueprint $table) {
            $table->text('flat_number')->after('address_alias')->nullable();
			$table->text('floor_number')->after('flat_number')->nullable();
			$table->text('area')->after('floor_number')->nullable();
			$table->text('block_number')->after('area')->nullable();
			$table->text('road_number')->after('block_number')->nullable();
			$table->text('additional_information')->after('road_number')->nullable();
			$table->boolean('is_default')->default(0)->after('additional_information');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('address_details', function (Blueprint $table) {
            //
        });
    }
}
