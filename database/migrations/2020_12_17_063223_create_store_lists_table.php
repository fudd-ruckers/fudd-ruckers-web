<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_lists', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->unsignedBigInteger('store_id')->index();
			$table->String('name')->nullable();
			$table->String('address')->nullable();
			$table->unsignedBigInteger('number');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_lists');
    }
}
